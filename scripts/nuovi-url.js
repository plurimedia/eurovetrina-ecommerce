/*
 * Crea un cantiere per ogni commessa di tipo 2
 */






var CONFIG = require(process.cwd() + '/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    slug = require('slugger'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    AliasSchema = require(process.cwd() + '/server/models/alias'),
    Alias = mongoose.model('Alias');



mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log('0 - Connesso al db', db)

    Prodotto.find({}, function(err,prodotti) {

    	console.log('Aggiorno gli url dei prodotti')

    	async.each(prodotti, function (prodotto,cbProdotto) {

    		console.log('aggiorno il prodotto', prodotto.codice)

    		var slugIT = slug(prodotto.it.nome+'-'+prodotto.codice)

    		prodotto.it.url = slugIT;

    		if(prodotto.en && prodotto.en.url){
    			var slugEN = 'en/'+slug(prodotto.en.nome+'-'+prodotto.codice);
    			prodotto.en.url = slugEN;
    		}

    		prodotto.update(prodotto, function(err){
    			if(err){
    				cbProdotto(err)
    			}
    			else{
    				console.log('aggiornato il prodotto', prodotto.codice)
    				cbProdotto()
    			}
    		})

    	},function(err){

    		if(err)
    			throw err;

    		console.log('finito di aggiornare i prodotti, aggiorno gli alias')

    			console.log('FINE - disconnetto')
				mongoose.connection.close()
    	})

    })


});