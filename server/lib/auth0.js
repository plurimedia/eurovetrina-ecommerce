// libreria di utilità per le api di auth0
const axios = require('axios')

const options = {
  method: 'POST',
  url: `https://${process.env.AUTH0_DOMAIN}/oauth/token`,
  headers: { 'content-type': 'application/json' },
  data: {
    grant_type: 'client_credentials',
    client_id: process.env.AUTH0_MM_CLIENT_ID,
    client_secret: process.env.AUTH0_MM_CLIENT_SECRET,
    audience: `https://${process.env.AUTH0_DOMAIN}/api/v2/`
  }
  /* body: '{"client_id":"' + process.env.AUTH0_MM_CLIENT_ID + '","client_secret":"' + process.env.AUTH0_MM_CLIENT_SECRET + '","audience":"' + `https://${process.env.AUTH0_DOMAIN}/api/v2/` + '","grant_type":"client_credentials"}' */
}

const executeQuery = async (query) => {
  // try {
    const authResponse = await axios.request(options)
    let opts = {
      method: 'GET',
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/${query}`,
      headers: { 'content-type': 'application/json', authorization: 'Bearer ' + authResponse.data.access_token }
    }

    const response = await axios.request(opts)
    if (response.data) {
      // logs = logResponse.data
      // console.log(logResponse.data)
      // console.log('recuperati ' + response.data.length + ' record su auth0')
      return response.data
    }
  // } catch (err) {
    // console.error(err)
  // }
}

const postObject = async (query, object) => {
  try {
    const authResponse = await axios.request(options)
    let opts = {
      method: 'POST',
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/${query}`,
      headers: { 'content-type': 'application/json', authorization: 'Bearer ' + authResponse.data.access_token },
      data: object
    }

    const response = await axios.request(opts)
    if (response.data) {
      // logs = logResponse.data
      // console.log(logResponse.data)
      // console.log('recuperati ' + response.data.length + ' record su auth0')
      return response.data
    }
  } catch (err) {
    console.error(err)
  }
}

const patchObject = async (query, object) => {
  const pick = function (obj, ...props) {
    // Riceve un oggetto e un elenco di properties
    // Ritorna un nuovo oggetto con solo le proprietà elencate
    const result = {}
    props.forEach(function (prop) {
      result[prop] = obj[prop]
    })
    return result
  }
  try {
    const toSave = pick(object, 'app_metadata')

    const authResponse = await axios.request(options)
    let opts = {
      method: 'PATCH',
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/${query}`,
      headers: { 'content-type': 'application/json', authorization: 'Bearer ' + authResponse.data.access_token },
      data: toSave
    }

    const response = await axios.request(opts)
    if (response.data) {
      // logs = logResponse.data
      // console.log(logResponse.data)
      // console.log('recuperati ' + response.data.length + ' record su auth0')
      return response.data
    }
  } catch (err) {
    console.error(err)
  }
}

const deleteObject = async (query) => {
  try {
    const authResponse = await axios.request(options)
    let opts = {
      method: 'DELETE',
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/${query}`,
      headers: { 'content-type': 'application/json', authorization: 'Bearer ' + authResponse.data.access_token }
    }

    const response = await axios.request(opts)
    // if (response.data) {
    // logs = logResponse.data
    // console.log(logResponse.data)
    // console.log('recuperati ' + response.data.length + ' record su auth0')
    return { message: 'ok' }
    // }
  } catch (err) {
    console.error(err)
  }
}

module.exports = {
  patchObject,
  postObject,
  executeQuery,
  deleteObject
}
