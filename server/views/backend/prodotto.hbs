<script src="/backend/js/controllers/prodotto.js"></script>

<div ng-controller="prodotto">

	<div class="row wrapper border-bottom white-bg page-heading">
	    <div class="col-lg-12">
	        <h2 ng-bind="pageTitle"></h2>
			<ol class="breadcrumb">
	        	<li><a href="/admin/dashboard">Home</a></li>
	        	<li><a href="/admin/prodotti">Prodotti</a></li>
	        	<li class="active"><strong ng-bind="pageTitle"></strong></li>
	    	</ol>
	    </div>
	</div>	

	<div class="wrapper wrapper-content">
		<div class="row" ng-cloak>
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-content">


						<div class="alert alert-warning" ng-show="categorie.$resolved && !categorie.voci.length">
							<a href="/admin/categoria" class="alert-link">Inserisci almeno una categoria</a> prima di inserire un prodotto
						</div>
						<!-- @@@@@@@@ contenuto controller @@@@@@@@ -->
						<form name="formProdotto" novalidate ng-show="categorie.$resolved && categorie.voci.length">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="codice">Codice *</label>
						                    <div class="input-group">
												<input type="text" class="form-control" ng-model="prodotto.codice" id="codice" autocomplete="off" required>
						                        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
						                    </div>	 							
											
										</div>						
									</div>
									<div class="col-md-6">
						                <div class="form-group">
						                	<label for="categoria">Categoria *</label>
						                	<select ng-options="categoria._id as categoria.nome group by categoria.group for categoria in categorieGroups | filter: filterOptions | orderBy : 'group'" 
						                		    ng-model="prodotto.categoria" class="form-control" 
						                		    required>
						                		<option value="">- Scegli -</option>
						                	</select>
						                </div>					
									</div>
								</div>

								<hr class="hr-line-dashed" />
								
								<div class="row">

									<div class="col-md-4">
										<div ng-if="prodotto.immagine" class="m-b-md">
							 				<cloudinary  id="[[prodotto.immagine]]" transformation="h_250,c_fill" bootstrapclass="img-responsive"></cloudinary>				
										</div>

										<div class="m-b-md clearfix" ng-if="prodotto.gallery.length">
												<div class="pull-left m-b-sm m-r-sm" ng-repeat="img in prodotto.gallery">
							 						<cloudinary id="[[img]]" transformation="w_100,h_100,c_fill" bootstrapclass="img-responsive"></cloudinary>
							 						<a href="" class="text-danger" ng-click="cancellaImmagine($index)"><i class="fa fa-trash"></i></a>								
												</div>
										</div>

										<div class="progress progress-striped active m-b-md" ng-if="progress">
										    <div style="width: [[progress]]%" class="progress-bar progress-bar-success">
										    </div>
										</div>

										<div class="dropzone dz-clickable m-b-lg" 
										     ng-if="!progress"
											 ng-model="files"
											 ngf-select="uploadImmagine($files)"
								             ngf-drop="uploadImmagine($files)"
								             ngf-drag-over-class="{accept:'ok', reject:'error'}"
								             ngf-multiple="false"
								             ngf-allow-dir="false"
								             ngf-pattern="image/*">
								        	<div class="dz-default dz-message">
								        		<span><strong>Trascina qui la foto principale. </strong><br> (.jpg o .png)</span>
								        	</div>
								        </div>

										<div class="dropzone dz-clickable" 
										     ng-if="!progressimmagini"
											 ng-model="files"
											 ngf-select="uploadImmagini($files)"
								             ngf-drop="uploadImmagini($files)"
								             ngf-drag-over-class="{accept:'ok', reject:'error'}"
								             ngf-multiple="true"
								             ngf-allow-dir="false"
								             ngf-pattern="image/*">
								        	<div class="dz-default dz-message">
								        		<span><strong>Trascina qui altre foto. </strong><br> (.jpg o .png)</span>
								        	</div>
								        </div>
									</div>


									<div class="col-md-8">

										<ul class="nav nav-pills admin-tabs">
											<li ng-class="{'active': lang === 'it'}"><a href="" ng-click="setIT()"><img src="/backend/img/icons/it.png">  Italiano</a></li>
											<li ng-class="{'active': lang === 'en'}"><a href="" ng-click="setEN()"><img src="/backend/img/icons/gb.png"> Inglese</a></li>
						                </ul>

						                <div class="m-t-lg" ng-show="lang==='it'">
							                <div class="form-group">
							                    <label for="nome">Nome *</label>
							                    <input type="text" class="form-control" ng-model="prodotto.it.nome" id="nome" autocomplete="off" required>
							                </div>
							                <div class="hr-line-dashed"></div>
							                <label for="prezzo">Prezzo *</label>
						                    <div class="input-group">
						                        <input type="number" 
						                               min="0" 
						                               class="form-control" 
						                               ng-model="prodotto.it.prezzo" 
						                               required
						                               >
						                        <span class="input-group-addon"><i class="fa fa-euro"></i></span>
						                    </div>	            
											<div class="hr-line-dashed"></div>

							                <div class="form-group">
							                    <label for="descrizione">Descrizione</label>
							                    <input type="text" class="form-control" ng-model="prodotto.it.descrizione" id="descrizione" autocomplete="off" >
							                </div>
							                <div class="hr-line-dashed"></div>

							                <div class="form-group">
							                    <label for="metatitle">Meta Title</label>
							                    <input type="text" class="form-control" ng-model="prodotto.it.meta_title" id="metatitle" autocomplete="off">
							                </div>
							                
							                <div class="hr-line-dashed"></div>	

							                <div class="form-group">
							                    <label for="metadesc">Meta Description</label>
							                    <input type="text" class="form-control" ng-model="prodotto.it.meta_description" id="metadesc" autocomplete="off">
							                </div>

							                {{#inArray session.passport.user.app_metadata.roles "admin"}}

							                <div class="hr-line-dashed"></div>

							                <div class="form-group">
							                    <label for="redirect">Vecchio url da reindirizzare</label>
							                    <input type="text" class="form-control" ng-model="prodotto.it.redirect" id="redirect" autocomplete="off">
							                </div>

							                {{/inArray}}

							       			<!-- varianti -->
							                <div class="m-t-lg" ng-if="!prodotto.it.varianti.length">
							                	<a ng-click="modalVariante.show()" class="btn btn-primary btn-outline"><i class="fa fa-plus"></i> Aggiungi varianti</a>
							                </div>
							                
							                <div class="row" ng-if="prodotto.it.varianti.length" ng-repeat="variante in prodotto.it.varianti"  ng-init="varianteIndex = $index">

							                	<div class="col-md-12 m-t-lg">
							                		<!--p class="lead">[[ variante.tipo ]]</p-->
							                		<p class="lead">Varianti</p>
							                	</div>

							                	<div class="col-md-12">
								                	<div class="row" ng-repeat="opzione in variante.opzioni">
									                	<div class="col-md-6">
											                <div class="form-group">
											                    <label for="Nome">Nome *</label>
											                    <input type="text" class="form-control" ng-model="opzione.nome" id="" autocomplete="off" required>
											                </div>	                		
									                	</div>
									                	<div class="col-md-4">
											                <div class="form-group">
											                    <label for="Nome">Prezzo *</label>
											                    <div class="input-group">
											                    	<input type="number" min="0" class="form-control" ng-model="opzione.prezzo" id="" autocomplete="off" required>
											                    	<span class="input-group-addon"><i class="fa fa-euro"></i></span>
											                    </div>  
											                </div>	                		
									                	</div>
									                	<div class="col-md-2">
											                <div class="form-group">
											                    <label class="visually-hidden d-block">elimina</label>
																<a class="btn btn-danger btn-outline" ng-click="eliminaOpzione(varianteIndex,$index)"><i class="fa fa-trash-o"></i></a>
											                </div>		                	
									                		
									                	</div>	                		
								                	</div>
							                	</div>



							                	<div class="col-md-12"><a href="" ng-click="nuovaOpzione(varianteIndex)">+ Nuova opzione</a></div>

							                </div>
							                <!--/varianti -->

						                </div>
						                <!--/italiano-->

						                <div class="m-t-lg" ng-show="lang==='en'">
							                <div class="form-group">
							                    <label for="nome">Nome *</label>
							                    <input type="text" class="form-control" ng-model="prodotto.en.nome" id="nome" autocomplete="off">
							                </div>
							                <div class="hr-line-dashed"></div>
							                <label for="prezzo">Prezzo *</label>
						                    <div class="input-group">
						                        <input type="number" 
						                               min="0" 
						                               class="form-control" 
						                               ng-model="prodotto.en.prezzo" >
						                        <span class="input-group-addon"><i class="fa fa-euro"></i></span>
						                    </div>	            
											<div class="hr-line-dashed"></div>

							                <div class="form-group">
							                    <label for="descrizione">Descrizione</label>
							                    <input type="text" class="form-control" ng-model="prodotto.en.descrizione" id="descrizione" autocomplete="off">
							                </div>

							                <div class="hr-line-dashed"></div>

							                <div class="form-group">
							                    <label for="metatitle">Meta Title</label>
							                    <input type="text" class="form-control" ng-model="prodotto.en.meta_title" id="metatitle" autocomplete="off">
							                </div>
							                <div class="hr-line-dashed"></div>	

							                <div class="form-group">
							                    <label for="metadesc">Meta Description</label>
							                    <input type="text" class="form-control" ng-model="prodotto.en.meta_description" id="metadesc" autocomplete="off">
							                </div>

							                {{#inArray session.passport.user.app_metadata.roles "admin"}}

							                <div class="hr-line-dashed"></div>

							                <div class="form-group">
							                    <label for="redirect">Vecchio url da reindirizzare</label>
							                    <input type="text" class="form-control" ng-model="prodotto.en.redirect" id="redirect" autocomplete="off">
							                </div>

							                {{/inArray}}

							       			<!-- varianti -->

							                <div class="m-t-lg" ng-if="!prodotto.en.varianti.length">
							                	<a ng-click="modalVariante.show()" class="btn btn-primary btn-outline"><i class="fa fa-plus"></i> Aggiungi varianti</a>
							                </div>
							                
							                <div class="row" ng-if="prodotto.en.varianti.length" ng-repeat="variante in prodotto.en.varianti" ng-init="varianteIndex = $index">

							                	<div class="col-md-12 m-t-lg">
							                		<!--p class="lead">[[ variante.tipo ]]</p-->
							                		<p class="lead">Varianti</p>
							                	</div>

							                	<div class="col-md-12">
								                	<div class="row" ng-repeat="opzione in variante.opzioni">
									                	<div class="col-md-6">
											                <div class="form-group">
											                    <label for="Nome">Nome *</label>
											                    <input type="text" class="form-control" ng-model="opzione.nome" id="" autocomplete="off" required>
											                </div>	                		
									                	</div>
									                	<div class="col-md-4">
											                <div class="form-group">
											                    <label for="Nome">Prezzo *</label>
											                    <div class="input-group">
											                    	<input type="number" min="0" class="form-control" ng-model="opzione.prezzo" id="" autocomplete="off" required>
											                    	<span class="input-group-addon"><i class="fa fa-euro"></i></span>
											                    </div>  
											                </div>	                		
									                	</div>
									                	<div class="col-md-2">
											                <div class="form-group">
											                    <label class="visually-hidden d-block">elimina</label>
																<a class="btn btn-danger btn-outline" ng-click="eliminaOpzione(varianteIndex,$index)"><i class="fa fa-trash-o"></i></a>
											                </div>		                	
									                		
									                	</div>	                		
								                	</div>
							                	</div>

							                	<div class="col-md-12"><a href="" ng-click="nuovaOpzione(varianteIndex)">+ Nuova opzione</a></div>

							                </div>
							                <!--/varianti -->
						                </div>
						                <!--/inglese-->
										
									</div>
								</div>	


								<div class="row">
									<div class="col-md-12">
										<div class="hr-line-dashed"></div>
										
										<div class="pull-left" ng-if="prodotto._id">
											<a class="btn btn-danger" ng-if="prodotto._id" ng-click="eliminaProdotto()">Elimina</a>
											{{#inArray session.passport.user.app_metadata.roles "admin"}}
											<a href="/admin/prodotto/[[prodotto._id]]/alias" class="btn btn-default m-l"><i class="fa fa-link"></i> Mostra gli alias</a>
											{{/inArray}}
										</div>


										<div class="pull-right">
						             		<label for="visibile" class="p-w-xs">
						                   		<input type="checkbox" ng-model="prodotto.visibile" id="visibile" /> Visibile sul sito
						                    </label>
						                	<button ng-click="salvaProdotto(true)" class="btn btn-primary" ng-disabled="disabled()" ng-if="!prodotto._id">Salva e continua</button>
											<button ng-if="prodotto._id" ng-click="duplicaProdotto()" class="btn btn-default"><i class="fa fa-copy"></i> Duplica</button>
						                    <button ng-click="salvaProdotto()" class="btn btn-primary" ng-disabled="disabled()">Salva</button>

						                </div>
										
									</div>
								</div>
							</form>
							<!-- @@@@@@@@ /contenuto controller @@@@@@@@ -->

					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

	

	
		






