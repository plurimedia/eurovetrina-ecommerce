"use strict";

var CONFIG = require(process.cwd() + '/config'),
    Mail = require(process.cwd() + '/server/mail'),
    express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    request = require('request'),
    _ = require('lodash'),
    Alias = mongoose.model('Alias'),
	Categoria = mongoose.model('Categoria'),
    Prodotto = mongoose.model('Prodotto'),
    CategorieEvidenza = mongoose.model('CategorieEvidenza'),
    MenuCategorie = mongoose.model('MenuCategorie'),
    Ordine = mongoose.model('Ordine'),
    Contatto = mongoose.model('Contatto'),
    CONFIG = require(process.cwd() + '/config'),
	UTILS = require(process.cwd() + '/server/utils'),
	logger = require(process.cwd() + '/server/logger'),
    ipn = require('paypal-ipn'),
	router = express.Router(),
    auth0 = require('../lib/auth0')

// recupera e cerca gli alias
router.get('/secure/alias', function (req, res, next) {

    var query = {};

    if (req.query.document) // sto cercando per documento
        _.extend(query,{document: req.query.document})
 
    Alias.find(query, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);
    });
})

// recupera e cerca le categorie
router.get('/secure/categoria', function (req, res, next) {

    var query = {};

    if (req.query.nome) // sto cercando per nome
        _.extend(query,{cerca_nome: {$regex: req.query.nome, $options:'$i'}})
 
    Categoria.find(query, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);
    });
})

// recupera una categoria
router.get('/secure/categoria/:id', function (req, res, next) {

    Categoria.findOne({_id: req.params.id}, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);
    });
})

// salva una categoria
router.post('/secure/categoria', function (req, res, next){

    var categoria = new Categoria(req.body);
    categoria.mdate = new Date();
 
    categoria.save(function (err, result) {
        if (err){
            logger.error(err)
            return next(err);
        }
        else
            res.status(200).send(result);
    });
})

// aggiorna una categoria
router.put('/secure/categoria/:id', function (req, res, next){
	
    var Mcategoria = req.body;
    Mcategoria.mdate = new Date();

    Categoria.findById(req.params.id, function (err, categoria) {
        categoria.update(Mcategoria, {
            runValidators: true // per il middleware
        }, function (err) {
            if (err) {
                logger.error(err)
                return next(err);
            } else {
                res.status(200).send(Mcategoria);
            }
        })
    });
})


// recupera e cerca il menu delle categorie
router.get('/secure/menucategorie', function (req, res, next){
 
    MenuCategorie.findOne({}, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);
    });
})


// aggiorna il menu categorie
router.put('/secure/menucategorie/:id', function (req, res, next){
    
    var Mmenu = req.body;
    Mmenu.mdate = new Date();

    MenuCategorie.findById(req.params.id, function (err, menu) {
        menu.update(Mmenu, {
            runValidators: true // per il middleware
        }, function (err) {
            if (err) {
                logger.error(err)
                return next(err);
            } else {
                res.status(200).send(Mmenu);
            }
        })
    });
})


// recupera e cerca le categorie in evidenza
router.get('/secure/categorievidenza', function (req, res, next){
 
    CategorieEvidenza.findOne({}, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);
    });
})

// salva le categorie in evidenza
router.post('/secure/categorievidenza', function (req, res, next){

    var evidenza = new CategorieEvidenza(req.body);
 
    evidenza.save(function (err, result) {
        if (err){
            logger.error(err)
            return next(err);
        }
        else
            res.status(200).send(result);
    });
})


// aggiorna le categorie in evidenza
router.put('/secure/categorievidenza/:id', function (req, res, next){
    
    var Mmenu = req.body;

    CategorieEvidenza.findById(req.params.id, function (err, menu) {
        menu.update(Mmenu, {
            runValidators: true // per il middleware
        }, function (err) {
            if (err) {
                logger.error(err)
                return next(err);
            } else {
                res.status(200).send(Mmenu);
            }
        })
    });
})

// recupera e cerca i prodotti
router.get('/secure/prodotto', function (req, res, next) {

    var query = {},
        limit = 0;

    if(req.query.keyword)
        _.extend(query,{keywords:{ $all: req.query.keyword.split(' ')}})

    if(req.query.categoria)
        _.extend(query,{categoria:req.query.categoria})

    if(req.query.sort) { // sort='peso,1'
        var sort = {};
        sort[req.query.sort.split(',')[0]] = parseInt(req.query.sort.split(',')[1]);
    }

    if(req.query.limit) { // limit='2'
        limit = parseInt(req.query.limit);
    }

    Prodotto.find(query)
            .populate('categoria')
            .limit(limit)
            .sort(sort)
            .exec(function(err,result){

                if (err){
                    logger.error(err)
                    return next(err);
                }

                res.status(200).send(result);
            })
})

// recupera un prodotto
router.get('/secure/prodotto/:id', function (req, res, next) {

    Prodotto.findOne({_id: req.params.id}, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);
    });
})

// salva un prodotto
router.post('/secure/prodotto', function (req, res, next){

    var prodotto = new Prodotto(req.body);
    prodotto.mdate = new Date();
 
    prodotto.save(function (err, result) {
        if (err){
            logger.error(err)
            return next(err);
        }
        else
            res.status(200).send(result);
    });
})

// aggiorna un prodotto
router.put('/secure/prodotto/:id', function (req, res, next){
    
    var Mprodotto= req.body;
    Mprodotto.mdate = new Date();

    Prodotto.findById(req.params.id, function (err, prodotto) {
        prodotto.update(Mprodotto, {
            runValidators: true // per il middleware
        }, function (err) {
            if (err) {
                logger.error(err)
                return next(err);
            } else {
                res.status(200).send(Mprodotto);
            }
        })
    });
})

// elimina un prodotto
router.delete('/secure/prodotto/:id', function (req, res, next){

    Prodotto.remove({
        _id: req.params.id
    }, function (err) {
        if (err){
            logger.error(err)
            return next(err);
        }
        else
            res.status(200).end();
    });
})

// aggiunge prodotto al carrello
router.post('/cart', function(req, res, next) {

    logger.verbose('Aggiorno il carrello')

    try {

        if(!req.session.cart)
            req.session.cart = { prodotti: [], count: 0}; 

        var item = req.body,
            found = _.find(req.session.cart.prodotti, function(c) {  // se esiste già
                        return c.opzione ? c.codice == item.codice && c.opzione == item.opzione : c.codice === item.codice
                    });

        if(!found) {
            req.session.cart.prodotti.push(item)
            logger.verbose('Aggiunto il prodotto %s al carrello', item.nome)
        }
        else {
            found.qta = found.qta + item.qta; 
             logger.verbose('Aggiornata la quantità del prodotto %s', item.nome) 
        }

        req.session.cart.count = req.session.cart.prodotti.length ? _.sumBy(req.session.cart.prodotti,'qta') : 0  

        logger.verbose('Finito di aggiornare il carrello %j', req.session.cart)

        res.send(req.session.cart);
    }
    catch (err) {
        logger.error('Errore aggiunta prodotto al carrello %j', err)
        res.status(500).end()
    }
})

// recupera il carrello
router.get('/cart', function(req, res, next) {
    res.send(req.session.cart);
})

// aggiorna il carrello
router.put('/cart', function(req, res, next) {
    req.session.cart = req.body;
    req.session.cart.count = req.session.cart.prodotti.length ? _.sumBy(req.session.cart.prodotti,'qta') : 0;
    res.send(req.session.cart);
})

// crea un utente su auth0
router.post('/user', async function(req, res, next) {
    const createEmailVerificationTicket = async (id, obj) => {
        return await auth0.postObject('tickets/email-verification', obj)
    }

    var user = req.body,
        user_metadata = _.omit(user,['email','password']).user_metadata,
        auth0User = {
          connection: CONFIG.AUTH0_CONNECTION,
          email: user.email,
          password: user.password,
          user_metadata: user_metadata,
          app_metadata: {
            roles: ['authenticated'],
          }
        };

    logger.verbose('Salvo utente su auth0, utente: %j', auth0User)

    // creo un utente su auht0
    try {
        const user = await auth0.postObject('users', auth0User)

        var data = {
            user_id: user.user_id,
            result_url: CONFIG.URL + 'api/auth/login?dest=carrello-pagamento'
        };
        try {
            const result = await createEmailVerificationTicket(user.user_id, data)

            logger.verbose('Utente creato su auth0, utente: %s, token email %s',user.user_id, result.ticket)
            req.session.usertoken = result.ticket;

            // mando la mail di verifica
            var subject = 'Verifica il tuo account';

            logger.verbose('Invio la mail di verifica a %s', user.email)

            Mail.send('verifica-utente',result,subject,[CONFIG.EMAIL_FROM],[user.email],null,null,function(err, mail) {

                if(err) {
                    logger.error('Non sono riuscito a inviare la mail di verifica %j',err)
                    return next(err)
                }

                logger.verbose('Inviata mail di verifica a %s',user.email)
                res.end()
            })
        } catch (err) {
            return next(err)
        }
    } catch (err) {
        logger.error('errore durante la creazione utente %j', err)
        console.error('errore durante la creazione utente', err)
        return res.status(500).send(err.message);
    }



    /* req.auth0Client.createUser(auth0User, function (err, user) {

        if (err){
            logger.error('errore durante la creazione utente %j', err)
            console.error('errore durante la creazione utente', err)
            return res.status(500).send(err.message);
        }

        var data = {
          user_id: user.user_id,
          result_url: CONFIG.URL + 'api/auth/login?dest=carrello-pagamento'
        };

        req.auth0Client.createEmailVerificationTicket(data, function (err, result) {
            if (err) {
                return next(err)
            }
            else {

                logger.verbose('Utente creato su auth0, utente: %s, token email %s',user.user_id, result.ticket)
                req.session.usertoken = result.ticket;

                // mando la mail di verifica
                var subject = 'Verifica il tuo account';

                logger.verbose('Invio la mail di verifica a %s', user.email)

                Mail.send('verifica-utente',result,subject,[CONFIG.EMAIL_FROM],[user.email],null,null,function(err, mail) {

                    if(err) {
                        logger.error('Non sono riuscito a inviare la mail di verifica %j',err)
                        return next(err)
                    }

                    logger.verbose('Inviata mail di verifica a %s',user.email)
                    res.end()
                })
            }
        });
    }); */
})

// aggiorna un utente su auth0
router.put('/user/:id', async function(req, res, next) {

    if(req.session.passport.user) {
        console.log(req.body)
        
        var user = req.body,
            params = { id: req.params.id};

        logger.verbose('Aggiorno utente su auth0, utente: %j',params.id )

        // update utente su auht0
        try {
            await auth0.patchObject('users/' + req.body.id, req.body)
            logger.verbose('Utente aggiornato su auth0, utente: %j',user)
            req.session.passport.user = user;
            res.send(user)
        } catch (err) {
            if (err){
                logger.error('errore durante l\'aggiornamento utente %j', err)
                return next(err);
            }
        }
        /* req.auth0Client.updateUserMetadata(params,user.user_metadata, function (err, user) {

            if (err){
                logger.error('errore durante l\'aggiornamento utente %j', err)
                return next(err);
            }
            
            logger.verbose('Utente aggiornato su auth0, utente: %j',user)
            req.session.passport.user = user;
            res.send(user)
        }); */
    }
    else {
        res.redirect('/403')
    }
})

// rotta di test per vedere se il client di auth0 funziona. COMMENTARE dopo lo sviluppo e non cancellare
router.get('/users', async function(req, res, next) {
    // replica init client come in middleware/auth0.js
    // messo qui perchè non passo dal middleware in questa rotta di test
    /* var ManagementClient = require('auth0').ManagementClient,
      auth0Client = new ManagementClient({
      token: CONFIG.AUTH0_TOKEN,
      domain: CONFIG.AUTH0_DOMAIN
    })
    req.auth0Client = auth0Client
    if (req.auth0Client)
        req.auth0Client.getUsers({ per_page: 100 }, function (err, users) {
            res.status(200).send({ err, users });
        })
    else res.status(200).send('ERRORE CLIENT'); */
    const ret = await auth0.executeQuery('users?page=0&per_page=100&include_totals=true&sort=username:-1&search_engine=v3')
    res.status(200).send(ret)
})

// recupera il carrello
router.get('/user', function(req, res, next) {
    res.send(req.session.passport ? req.session.passport.user : undefined);
})

// salva un ordine
router.post('/order', function(req, res, next) {

    logger.verbose('Salvo un ordine') 

    var ordine = {
        carrello: req.session.cart,
        utente: req.session.passport.user,
        stato: 1
    }


    if(req.session.passport.user) {

        var ordine = new Ordine(ordine);

        logger.verbose('Salvo ordine %j', ordine)

        // 1) salvo l'ordine
        ordine.save(function (err, ordine) {

            if (err){
                logger.error(err)
                return next(err);
            }
            else {

                logger.verbose('Ordine salvato, invio le mail')
               
                ordine.mail_cliente = {};
                ordine.mail_commerciante = {};

                var ordineObj = ordine.toObject();
                ordineObj.carrello.prodotti.forEach(function(p){
                    p.fullurl = CONFIG.URL + p.url;
                })

                // 2) Mando le mail a cliente e commerciante
                async.waterfall([

                    function(cb){

                        logger.verbose('Invio mail al cliente')

                        var subject = 'Il tuo ordine su '+ CONFIG.TITLE

                        //type,data,subject,from,to,cc,ccn,callback
                        Mail.send('ordine-cliente',ordineObj,subject,[CONFIG.EMAIL_FROM],[req.session.passport.user.email],null,null,function(err, datimail1){
                            if(err)
                                cb({err: err, mail: 10})
                            cb(null,datimail1)
                        })
                    },
                    function(datimail1, cb) {

                        logger.verbose('Invio mail al commerciante')

                        var subject = 'Nuovo ordine sul sito'

                        Mail.send('ordine-commerciante',ordineObj,subject,[CONFIG.EMAIL_FROM],[CONFIG.EMAIL_FROM],null,null,function(err, datimail2){
                            if(err)
                                cb({err: err, mail: 20})
                            cb(null,datimail1, datimail2)
                        })
                    }
                ],
                function(err, datimail1, datimail2) {

                    // per qualche motivo non posso mandare una delle mail, lo segno nella mail
                    if(err) {

                        logger.error('Impossibile inviare una delle mail dell\'ordine %j', err.err)

                        if(err.mail===10)
                            ordine.mail_cliente = false;

                        if(err.mail===20)
                            ordine.mail_commerciante = false;
                    }

                    logger.verbose('Aggiorno ordine con lo stato delle mail')


                    Ordine.findById(ordine._id, function (err, ordine) {

                        var Mordine =_.extend(ordine, {mail_cliente:datimail1, mail_commerciante:datimail2})

                        ordine.update(Mordine, {
                            runValidators: true // per il middleware
                        }, function (err,ordine) {
                            if (err) {
                                logger.error(err)
                                return next(err);
                            } else {

                                logger.verbose('Ordine creato')
                                req.session.cart = { prodotti: [], count: 0 }; 

                                res.send(Mordine)
                            }
                        })
                    });
                })
            }
        });
    }
    else {
        res.redirect('/403')
    }
})

// ipn paypal
router.post('/ipn', function(req, res, next) {

    // dopo un pagamento, arriva la notifca paypal, devo fare questo delirio (uso la libreria node-ipn)
    // https://www.paypal.com/en/cgi-bin/webscr?cmd=p/acc/ipn-info-outside
    // per testare risposte ipn https://developer.paypal.com/developer/ipnSimulator/
    
    var rispostaPaypal = req.body;

    logger.verbose('1) Prima risposta paypal',rispostaPaypal)

    ipn.verify(rispostaPaypal, {'allow_sandbox': true}, function callback(err, message) {
        if(err){
            logger.verbose('Errore ipn paypal', err)
            return next(err)
        } 
        else {

            var numeroOrdine = rispostaPaypal.custom

            logger.verbose('2) Seconda risposta di paypal',message)

            logger.verbose('3) controllo e aggiorno ordine sul sito n°', numeroOrdine)

        
            Ordine.findOne({numero: numeroOrdine}, function(err,ordine){
                if(err){
                    logger.verbose('Ordine non trovato (chiamata IPN)')
                    return next(err)
                }
                else {

                    ordine.paypal = rispostaPaypal;

                    if(message.indexOf('VERIFIED')!==-1) {

                        logger.verbose('4) pagamento verificato, eseguo gli altri controlli')
                        logger.verbose('4a) totale ordine', ordine.carrello.totale)
                        logger.verbose('4b) totale ordine paypal', rispostaPaypal.mc_gross)
                        logger.verbose('4b) stato pagamento', rispostaPaypal.payment_status)
                 
                        if(ordine.carrello.totale != rispostaPaypal.mc_gross || rispostaPaypal.payment_status!='Completed') {

                            logger.verbose('5) Il totale non coincide o pagamento non verificato')
                            ordine.stato = -2;
                        }
                        else{
                            logger.verbose('5) ordine pagato correttamente')
                            ordine.stato = 2;
                        }
                    }
                    else {
                        logger.verbose('4) Errore paypal ipn')
                        ordine.stato = -1; // errore paypal
                    }

                    ordine.update(ordine, {
                        runValidators: true // per il middleware
                    }, function (err) {
                        if (err) {
                            logger.error(err)
                            return next(err);
                        } else {
                            res.status(200).end();
                        }
                    })
                }
            })
        }
    });
})

// recupera e cerca gli ordini
router.get('/secure/ordine', function (req, res, next) {

    var query = {},
        limit = 0;

    if(req.query.keyword)
        _.extend(query,{keywords:{ $all: req.query.keyword.split(' ')}})

    if(req.query.limit) { // limit='2'
        limit = parseInt(req.query.limit);
    }

    Ordine.find(query)
            .limit(limit)
            .sort({data:-1})
            .exec(function(err,result){

                if (err){
                    logger.error(err)
                    return next(err);
                }

                res.status(200).send(result);
            })
})

// recupera un ordine
router.get('/secure/ordine/:id', function (req, res, next) {

    Ordine.findOne({_id: req.params.id}, function (err, result) {

        if (err){
            logger.error(err)
            return next(err);
        }

        res.status(200).send(result);

    });
})

// aggiorna un ordine
router.put('/secure/ordine/:id', function (req, res, next){
    
    var Mordine = req.body;

    Ordine.findById(req.params.id, function (err, ordine) {
        ordine.update(Mordine, {
            runValidators: true // per il middleware
        }, function (err) {
            if (err) {
                logger.error(err)
                return next(err);
            } else {
                res.status(200).send(Mordine);
            }
        })
    });
})


// controlla lo stato di una mail
router.get('/secure/email/:id', Mail.stato)

// invia mail contatto
router.post('/contact', function (req, res, next) {

    var contatto = new Contatto(req.body);

    logger.verbose('Inserisco la richiesta di contatto %j', req.body)

    contatto.save(function(err, contatto){
        
        if(err)
            return next(err)

        logger.verbose('Invio la mail')

        Mail.send('contatto',req.body,'Contatto dal sito',[CONFIG.EMAIL_FROM],[CONFIG.EMAIL_FROM],null,null,function(err, mail){
            if(err){
                logger.error('Impossibile inviare la mail %j', err)
                return next(err)
            }
            res.end();
        })
    })
})

module.exports = router
