const express = require('express')
const router = express.Router()
const passport = require('passport')
const querystring = require('querystring')
const isAuth = require('../middleware/middlewares-auth0').isAuth
const isAdmin = require('../middleware/middlewares-auth0').isAdmin
// const sql = require('../db')

const parameterGuard = (req, res, next) => {
  if (req.query.code) {
    // per un bug su passport-auth0 se esiste questo parametro, restituisce "forbidden"
    // il parametro viene messo quando arriviamo dalla conferma di registrazione utente via mail
    delete req.query.code
    // metto la destinazione nella request per averla nella funzione successiva
    req.dest = req.query.dest
  }
  next()
}

router.get('/login', parameterGuard, passport.authenticate('auth0', { scope: 'openid email profile' }), (req, res) => {
  // console.log('api/login', req.session.passport, req.isAuthenticated())
  let returnTo = req.protocol + '://' + req.hostname
  returnTo =
    ((!process.env.NODE_ENV) || (process.env.NODE_ENV && process.env.NODE_ENV !== 'production'))
      ? `${returnTo}:3000/`
      : `${returnTo}/`
  
  if (req.dest) {
    returnTo += req.dest
  }
  // console.log('/login', returnTo)
  res.redirect(returnTo)
})

router.get('/callback', (req, res, next) => {
  passport.authenticate('auth0', async (err, user, info) => {
    // console.log('callback auth function', JSON.stringify(user), typeof user, err)
    // console.log('api/callback', req.session.passport, req.isAuthenticated())
    if (err) return next(err)

    if (!user) return res.redirect('/login')

    req.login(user, (err) => {
      // console.log('api/callback (2)', req.session.passport, req.isAuthenticated())
      if (err) return next(err)

      let returnTo = req.protocol + '://' + req.hostname

      returnTo =
        ((!process.env.NODE_ENV) || (process.env.NODE_ENV && process.env.NODE_ENV !== 'production'))
          ? `${returnTo}:3000/`
          : `${returnTo}/`

      if (user && user.roles && user.roles.length > 0 && user.roles[0] === 'authenticated') {
        // returnTo += 'carrello-consegna' 
      } else {
        returnTo += 'admin/dashboard'
      }
      res.redirect(returnTo)
    })
  })(req, res, next)
})

router.get('/logout', (req, res, next) => {
  // console.log('api/logout', req.session.passport, req.isAuthenticated())
  req.logout()
  // console.log('api/logout (2)', req.session.passport, req.isAuthenticated())

  let returnTo = req.protocol + '://' + req.hostname

  returnTo =
    ((!process.env.NODE_ENV) || (process.env.NODE_ENV && process.env.NODE_ENV !== 'production'))
      ? `${returnTo}:3000/`
      : `${returnTo}/`

  const logoutURL = new URL(`https://${process.env.AUTH0_DOMAIN}/v2/logout`)

  const searchString = querystring.stringify({ // The querystring API is considered Legacy. While it is still maintained, new code should use the URLSearchParams API instead
    client_id: process.env.AUTH0_CLIENT_ID,
    returnTo: returnTo
  })
  logoutURL.search = searchString

  res.redirect(logoutURL)
})

router.get('/is-authenticated', isAuth, async (req, res) => {
  res.status(200).json(req.session.passport)
})

router.get('/is-admin', isAdmin, async (req, res) => {
  res.status(200).json(req.session.passport)
})

router.get('/current-user', isAuth, async (req, res) => {
  res.status(200).json(req.user)
})

module.exports = router
