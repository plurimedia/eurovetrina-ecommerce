
exports.getUserInfoDb = async function (id, done) {
  let sql
  try {
    sql = require('../db.js')
    const [user] = await sql`
    select utente_id, admin, mail, attivo, nome
      from utenti
     where utente_id = ${id}
    `
    if (user) return done(null, user)

    return done('ID errato ', id)
  } catch (err) {
    // non faccio nulla, l'unico errore lo potrebbe dare il require
  }
}

exports.getUserInfo = async function (id, done) {
  // implementazione di una funzione che restituisce l'utente in base all'id
  // console.log('deserialize user', id)
  return done(null, { id: id, farlocco: true })
}
