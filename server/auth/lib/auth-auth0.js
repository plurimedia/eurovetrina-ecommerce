exports.authenticate = function (accessToken, refreshToken, extraParams, profile, done) {
  console.log('exports.authenticate', accessToken, refreshToken, extraParams, profile, done)
  profile.accessToken = extraParams.access_token
  profile.idToken = extraParams.id_token
  profile.app_metadata = profile._json.app_metadata
  profile.user_metadata = profile._json.user_metadata
  profile.roles = profile._json.app_metadata.roles
  return done(null, profile) // solo di esempio
}

exports.isAuthenticated = (req, res, next) => {
  if (req.user) return next()

  res.status(401).send('Unauthorized')
}
