const passport = require('passport')

// autenticazione auth0
const Auth0Strategy = require('passport-auth0')
// funzioni deserialize
// const auth = require('../lib/auth')

const authAuth0 = require('../lib/auth-auth0')

passport.use(new Auth0Strategy(
  {
    domain: process.env.AUTH0_DOMAIN,
    clientID: process.env.AUTH0_CLIENT_ID,
    clientSecret: process.env.AUTH0_CLIENT_SECRET,
    callbackURL: process.env.AUTH0_REDIRECT_URI,
    state: true
  }, authAuth0.authenticate
))

// tell passport how to serialize the user
/*
serializeUser determines which data of the user object should be stored in the session.
The result of the serializeUser method is attached to the session as req.session.passport.user = {}
*/
passport.serializeUser((user, done) => {
  console.log('serialize user', user)
  done(null, user)
})
/*
The first argument of deserializeUser corresponds to the key of the user object that was given to the
done function in serializeUser
*/
passport.deserializeUser((user, done) => {
  return done(null, user)
})

const authApi = require('../api/auth-auth0')

exports.passport = passport
exports.authApi = authApi
