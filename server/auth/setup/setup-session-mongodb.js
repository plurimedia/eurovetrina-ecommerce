const MongoStore = require('connect-mongo')
const path = require('path')

const sess = {
  secret: process.env.TOKEN_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 6000000, secure: false },
  store: MongoStore.create({
    mongoUrl: process.env.MONGOURL,
    ttl: 6000000,
    autoRemove: 'native',
    /* mongoOptions: {
      sslValidate: true,
      tls: true,
      tlsCAFile: path.join(__dirname, '../../../ibm_mongodb_ca.pem')
    } */
  })
}

exports.sess = sess
/* if (process.env.NODE_ENV !== 'development') {
  sess.cookie.secure = true // serve secure cookies if we are on https
} */
