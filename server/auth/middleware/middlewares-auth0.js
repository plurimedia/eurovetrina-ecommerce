module.exports.isAuth = (req, res, next) => {
  // console.log('middleware/isAuth', req.session.passport, req.isAuthenticated())
  if (req.isAuthenticated()) next()
  else res.status(401).json({ msg: 'Questa risorsa è riservata agli utenti autorizzati' })
}

module.exports.isAdmin = (req, res, next) => {
  if (req.isAuthenticated() && req.user.admin === 1) next()
  else res.status(401).json({ msg: 'Questa risorsa è riservata agli utenti Amministratori' })
}
