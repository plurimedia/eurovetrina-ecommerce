/*
 *
 * Funzioni di utilità generali
 */

"use strict";

var _ = require('lodash'),
    numeral = require('numeral'),
    path = require('path'),
    CONFIG = require(process.cwd() + '/config'),
    logger = require(process.cwd() + '/server/logger'),
    i18n = require('i18n');


numeral.register('locale', 'it', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    currency: {
        symbol: '€'
    }
});

numeral.locale('it');


module.exports = {


    page500: function(err, req, res, next) {

        if(CONFIG.dev) {
            err.show = true;
        }

        res.status(500).render('500', {
            title: 'Qualcosa è andato storto',
            slogan: CONFIG.title,
            description: 'errore',
            layout: 'frontend/400',
            error: err
        });
    },

    page404: function(req, res, next) {

        logger.error('404 Page not found: %s', req.url);

        res.status(404).render('404', {
            title: 'Pagina non trovata :(',
            slogan: CONFIG.title,
            description: 'La pagina che hai richiesto non esiste',
            path: req.url,
            layout: 'frontend/400'
        });
    },

    cloudinaryUrl : function (id,transformation){
        if(id && transformation) {

            var url = 'https://res.cloudinary.com/'+CONFIG.CLOUDINARY_CLOUD_NAME+'/image/upload/';
            url += transformation+'/';
            url += encodeURI(id);

            return url;
        }
    },

    euro: function (value) {
        return numeral(value).format('$0,0.00');
    },

    keepCurentLang: function (obj,lang) {

        var output =  _.extend(obj,obj[lang]) // metto al primo livello;

        i18n.getLocales().forEach(function(l){
            delete output[l]
        })

        return output;
    },

    entityAliases: function (entity) {
        return {
            it: entity.it.url,
            en: entity.en ? entity.en.url : undefined
        }
    }
};