/*
 * Logger custom: manda in stampa i verbose in base alla proprietà verbose dell'ambiente in config
 */
const { createLogger, format, transports } = require('winston')

var CONFIG = require(process.cwd() + '/config.js'),
  logger = createLogger({
    format: format.combine(
      format.splat(),
      format.simple()
    ),
    transports: [
      new (transports.Console)({colorize: true})
    ]
  })

if (CONFIG.verbose) logger.level = 'verbose'

module.exports = logger
