/*
* ad ogni richiesta controlla l'url e decide come procedere
 */

var mongoose = require('mongoose'),
	Alias = mongoose.model('Alias'),
	CONFIG = require(process.cwd() + '/config'),
	UTILS = require(process.cwd() + '/server/utils'),
	logger = require(process.cwd() + '/server/logger');
 
module.exports = function (req,res,next) {

    var richiesta = req.path.replace('/',''),
        richiestaRaw = req.path,
        Alias = mongoose.model('Alias');

    logger.verbose('Middleware alias, richiesta la pagina %s',richiesta)


    if(richiesta == 'favicon.ico' || richiesta == '/favicon.ico')
        return next()

    Alias.findOne({url: richiesta }, function (err, alias) {

        if(err)
            return next(err)

        if(req.hostname === 'www.eurovetrina.it' || req.hostname === 'www.eurovetrinaespositori.com' && req.url === '/') {
            logger.verbose('Redirect %s al dominio principale', req.hostname )
            return res.redirect(CONFIG.URL)
        }

        // in test e produzione forza https
        if (req.headers['x-forwarded-proto'] !== 'https' && CONFIG.URL.indexOf('localhost')==-1) {
            logger.verbose('Reindirizzo alla home in versione https')
            return res.redirect(`https://${req.header('host')}${req.url}`)
        }

        if((richiesta.indexOf('api')!==-1 || richiesta.indexOf('admin')!==-1) && (richiestaRaw.indexOf('api/')!==-1 || richiestaRaw.indexOf('admin/')!==-1)){
            logger.verbose('%s è una api del backend, vado avanti',richiesta)
            next()
        }

        else if(!alias) {

            var _setLang = function (url) {
                if(url.indexOf('/en/')!==-1)
                    req.lang = 'en'
                else
                    req.lang = 'it'

            }



            if(richiesta === 'cambialingua') {
                logger.verbose('Richiesto il cambio lingua in %s',req.query.lang)
                if(req.query.url === '/' && req.query.lang === 'it')
                    res.redirect('/')
                else if(req.query.url === '/' && req.query.lang === 'en')
                    res.redirect('/en')
                else
                    res.redirect(req.query.url) // rimando dov'ero con la lingua giusta
            }

            else if (richiesta === '') {
            	logger.verbose('Ho richiesto la home')
                _setLang(req.url)
            	next()
            }

            else if (richiesta === 'login') {
                logger.verbose('Ho richiesto la login')
                _setLang(req.url)
                next()
            }

            else if (richiesta === 'user') {
                logger.verbose('Ho richiesto la pagina utente')
                _setLang(req.url)
                next()
            }

            else if (richiesta === 'auth0callback') {
                logger.verbose('Ho richiesto la callback della login')
                _setLang(req.url)
                next()
            }

            else if (richiesta === 'sitemap.xml') {
                logger.verbose('Ho richiesto la sitemap xml')
                _setLang(req.url)
                next()
            }

            else if (richiesta === 'cerca') {
                logger.verbose('Ho richiesto la ricerca')
                _setLang(req.url)
                next()
            }
            
            else {
                logger.verbose('%s è una pagina statica', richiesta)
                req.baseUrl = CONFIG.URL
                _setLang(req.url)
                next()            	
            }
        }

        else if (alias) {

            if(alias.redirect) {
               logger.warn('%s è un redirect verso %s',richiesta,alias.redirect)
               res.redirect(301,'/'+alias.redirect)
            }
            else {
                logger.verbose('%s non è un redirect vado avanti',alias.url) 
                req.lang = alias.lang;
                req.alias = alias;
                req.baseUrl = CONFIG.URL
                next()
            }
        }
        
    }).lean()

};