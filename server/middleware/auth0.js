/*
* 	richiede un token ad auth0 per per usare le api: creo il client e lo metto in request
 */

var CONFIG = require(process.cwd() + '/config'),
	request = require('request');
 
module.exports = function (req,res,next) {

  var options = { method: 'POST',
    url: 'https://plurimedia.eu.auth0.com/oauth/token',
    headers: { 'content-type': 'application/json' },
    body: '{"client_id":"' + CONFIG.AUTH0_CLIENT_ID + '","client_secret":"' + CONFIG.AUTH0_CLIENT_SECRET + '","audience":"https://plurimedia.eu.auth0.com/api/v2/","grant_type":"client_credentials"}' };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);

    var ManagementClient = require('auth0').ManagementClient,
      auth0Client = new ManagementClient({
        token: body.access_token,
        clientId: CONFIG.AUTH0_CLIENT_ID,
        clientSecret: CONFIG.AUTH0_CLIENT_SECRET,
        domain: CONFIG.AUTH0_DOMAIN
      })

    req.auth0Client = auth0Client;

    next()
  });
}
