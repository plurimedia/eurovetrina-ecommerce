/* rotte parte pubblica */


var CONFIG = require(process.cwd() + '/config'),
    UTILS = require(process.cwd() + '/server/utils'),
    _ = require('lodash'),
    i18n = require('i18n'),
    express = require('express'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    async = require('async'),
    sm = require('sitemap'),
    Alias = mongoose.model('Alias'),
    CategorieEvidenza = mongoose.model('CategorieEvidenza'),
    MenuCategorie = mongoose.model('MenuCategorie'),
    Categoria = mongoose.model('Categoria'),
    Prodotto = mongoose.model('Prodotto'),
	logger = require(process.cwd() + '/server/logger'),
	router = express.Router(),
    staticRoutes = {
        chisiamo: { it: '/chi-siamo', en: '/en/about-us'},
        dovesiamo: { it: '/dovesiamo', en: '/en/where-find-us'},
        contatti: { it: '/contatti', en: '/en/contacts'},
        condizioni: { it: '/condizioni-di-vendita', en: '/en/terms-and-conditions-sale'},
        catalogue: { it: '/ecatalogue', en: '/en/ecatalogue'},
        categorie: { it: '/categorie', en: '/en/categories'},
        legal: { it: '/legal', en: '/en/legal'},
        sitemap: { it: '/mappa', en: '/en/mappa'}
    },
    _getMenuCategorie = function (lang,cb) {

        logger.verbose('Recupero il menu delle categorie nella lingua %s',lang)
    
        MenuCategorie.findOne({}, function(err,menu) {

            if(err)
                cb(err)

            try {
                if(menu && menu.voci) {

                    // tengo solo le proprietà della lingua corrente
                    function parsaVoci (menu) {
                         menu.voci.forEach(function(voce) {
                            _.extend(voce,voce.categoria)
                            delete voce.categoria;
                            UTILS.keepCurentLang(voce,lang)
                            if(voce.voci && voce.voci.length)
                                parsaVoci(voce)
                        });               
                    } 

                    parsaVoci(menu); 
                    cb(null,menu.voci)
                }
            }

            catch(err) {
                logger.error('Errore durante la creazione dell\'albero delle categorie: %j', err)
                cb(err)
            }
        })

    },
    _getCategorieEvidenza = function (lang,cb) {

        logger.verbose('Recupero le categorie in evidenza nella lingua %s',lang)

        CategorieEvidenza.findOne({}, function(err, evidenza){

            if(err)
                cb(err)

            // tengo la lingua corrente
            try {

                if(evidenza && evidenza.categorie) {

                    evidenza.categorie.forEach(function(c) {

                        c.titolo = c[lang];
                        
                        if(c.figlie && c.figlie.length){
                            c.figlie.forEach(function(f){
                                UTILS.keepCurentLang(f,lang)
                                if(f.prodotti && f.prodotti.length) {
                                    f.prodotti.forEach(function(p){
                                         UTILS.keepCurentLang(p,lang)
                                    })                                   
                                }
                            })
                        }
                    })
                }

                cb(null,evidenza)
            }
            catch(err){
                cb(err)
                logger.error('Errore durante il parsing delle categorie ine videnza', err)
            }


        }).lean()
    },
    _getCategoria = function (alias, lang, page, cb) {

        logger.verbose('Recupero la pagina %s della categoria %s ',page, alias.url)

        async.waterfall([

            function(callback) {
                Categoria.findOne({_id:alias.document}, function(err, categoria){

                    if(err)
                        callback(err)

                    if(categoria) {
                        logger.verbose('Trovato la categoria %s', categoria._id)
                        callback(null, categoria)
                    }
                    else {
                        callback('categoria non trovata', {})
                    }

                }).lean() 
            },

            function(categoria,callback) {

                Prodotto.count({categoria: categoria._id}, function (err, count){
                    if(err)
                        callback(err)

                    logger.verbose('Ho un totale di %s prodotti nella categoria %s',count,categoria._id)

                    callback(null,categoria,count)
                })

            },

            function(categoria, count, callback) {

                logger.verbose('Trovo i prodotti della pagina %s della categoria %s', page, categoria._id)

                Prodotto.find({categoria: categoria._id}, function(err, prodotti) {

                    if(err)
                        callback(err)

                     logger.verbose('Trovati %s prodotti', prodotti.length)

                     _.extend(categoria,{prodotti: prodotti, pages: Math.ceil(count/CONFIG.PAGINATE)})

                    callback(null, categoria)

                })
                .limit(CONFIG.PAGINATE)
                .skip(CONFIG.PAGINATE * page)
                .sort({peso: 1})
                .lean()
            }
        ],
        function(err, categoria) {

            if(err){
                cb(err)
            }
            else {
                 logger.verbose('Pulisco la categoria per il frontend')
                // pulisco
                
                categoria.langs = UTILS.entityAliases(categoria)
                categoria = UTILS.keepCurentLang(categoria,lang)
                    
                categoria.prodotti.forEach(function(p){
                    UTILS.keepCurentLang(p,lang);
                })


                cb(null,categoria)
            }
        });

       
    },
    _getProdotto = function (alias, lang, cb) {


        Prodotto
            .findOne({_id: alias.document})
            .populate('categoria')
            .lean()
            .exec(function (err, prodotto) {

                if(err)
                    cb(err)

                if(prodotto) {
                    logger.verbose('Trovato il prodotto %s',prodotto)
                    //logger.verbose('Trovato il prodotto %s',prodotto._id)

                    var categoria = UTILS.keepCurentLang(prodotto.categoria,lang);

                    prodotto.langs = UTILS.entityAliases(prodotto)
                    prodotto = UTILS.keepCurentLang(prodotto,lang)
                    prodotto.categoria_url = categoria.url;
                    prodotto.categoria_nome= categoria.nome;
                    prodotto.immagine_carrello = 'https://res.cloudinary.com/'+CONFIG.CLOUDINARY_CLOUD_NAME+'/image/upload/h_120,w_90,c_fill/'+prodotto.immagine;
                    // metto tutte le immagini in un unico array per la gallery
                    prodotto.immagini = [prodotto.immagine];
                    if(prodotto.gallery)
                        prodotto.immagini = _.union(prodotto.immagini,prodotto.gallery)

                    cb(null,prodotto)
                }
                else {
                    cb('prodotto non trovato', {})
                }

            });
    },
    _getSitemap = function (cb) {

        async.waterfall([

            function(callback) {
                MenuCategorie.findOne({}, function(err, MenuCategorie){

                    if(err)
                        callback(err)

                    var categorie = [];

                    _.map(MenuCategorie.voci,function(voce) {

                        if(voce.visibile) {
                            categorie.push({
                                url: voce.categoria.it.url,
                                changefreq: 'monthly',
                                priority: 0.5
                            })

                            if(voce.categoria.en) {
                                categorie.push({
                                    url: voce.categoria.en.url,
                                    changefreq: 'monthly',
                                    priority: 0.5
                                })     
                            }                       
                        }
                    })

                    callback(null,categorie)

                })
                .lean()
                 
            },

            function(categorie,callback) {

                Prodotto.find({visibile: true},{_id:-1}, function(err, prodotti){

                    if(err)
                        callback(err)

                    var Prodotti = [];

                    _.map(prodotti,function(p){

                        Prodotti.push({
                            url: p.it.url,
                            changefreq: 'monthly',
                            priority: 0.5
                        })

                        if(p.en && p.en.url) {
                            Prodotti.push({
                                url: p.en.url,
                                changefreq: 'monthly',
                                priority: 0.5
                            })
                        }
                    })


                    callback(null,categorie,Prodotti)

                })
                .select('it.url en.url')
                .lean() 

            }
        ],
        function(err, categorie, prodotti) {

            if(err){
                cb(err)
            }
            else {
                cb(null,categorie, prodotti)
            }
        });
    };

    
/* 403 */

router.get('/403', function(req,res,next){

    res.status(403).render('403', {
        title: 'Non autorizzato',
        slogan: CONFIG.title,
        description: 'errore 403',
        layout: 'frontend/400'
    });    
})

/* sitemap */
router.get('/sitemap.xml', function(req, res, next) {

    _getSitemap(function(err, categorie, prodotti){

        if(err)
            return next(err)

        var pagine = []

        _.map(staticRoutes, function (p){
            pagine.push({
                url: p.it,
                changefreq: 'monthly',
                priority: 0.5
            })
            pagine.push({
                url: p.en,
                changefreq: 'monthly',
                priority: 0.5
            })
        })

        logger.verbose('Renderizzo la sitemap: %s prodotti, %s categorie, %s pagine', prodotti.length, categorie.length, pagine.length)

        sitemap = sm.createSitemap ({
          hostname: CONFIG.URL,
          cacheTime: 600000,        // 600 sec - cache purge period
          urls: _.union(categorie, prodotti, pagine)
        }); 


        sitemap.toXML( function (err, xml) {

          if (err) {
            return res.status(500).end();
          }

          res.header('Content-Type', 'application/xml');
          res.send(xml);

        });
    })

});

//////// nuova auth0 page
router.get('/prova', function(req, res, next){
    var page = {
        layout: 'frontend/auth0',
    }
    res.render('frontend/prova', page);        
});

/* router.get('/user', function(req, res, next){

    var page = {
        layout: 'frontend/interna',
        title: 'Profilo',
        slogan: CONFIG.TITLE,
        lang: req.cookies.ev_i18n
    }

    if(!req.session.user) {
        res.redirect('/');
    }

    else{
        res.render('frontend/user', page);    
    }
}); */


/* rotte utente */

router.get('/login',
  function(req, res, next){

    var callback = req.query.dest ? CONFIG.AUTH0_CALLBACK_URL+'?dest='+req.query.dest : CONFIG.AUTH0_CALLBACK_URL,
        page = {
            layout: 'frontend/login',
            title: 'Accedi',
            slogan: CONFIG.TITLE,
            AUTH0_CALLBACK_URL: callback,
            AUTH0_CLIENT_ID: CONFIG.AUTH0_CLIENT_ID,
            AUTH0_DOMAIN: CONFIG.AUTH0_DOMAIN
    };

    if(!req.session?.passport?.user)
        res.render('frontend/login', page);
    else
        res.redirect('/user');
});

router.get('/logout', function(req, res, next){
  logger.verbose('Logout!')
  delete req.session.passport.user;
  delete req.session.cart;
  req.logout();
  res.redirect('/');
});

/* router.get('/auth0callback',
  passport.authenticate('auth0', { failureRedirect: '/url-if-something-fails' }),
  function(req, res) {
    const namespace = 'https://brianzacque-backend-it/' // See Auth0 rules
    
    // var roles = req.user._json.roles;
    if(!req.user._json.email_verified)
        res.redirect('/403')
    else {
        req.session.user = req.user._json;
        req.session.user.roles = req.session.user[namespace + 'app_metadata'].roles
        req.session.user.tenant = req.session.user[namespace + 'app_metadata'].tenant
        req.session.user.app_metadata = req.session.user[namespace + 'app_metadata']
        req.session.user.user_metadata = req.session.user[namespace + 'user_metadata']

        if(req.session.user.roles && req.session.user.roles.indexOf('redattore')!==-1)
            res.redirect('/admin/dashboard');
        else {
            if(req.query.dest)
                res.redirect('/'+req.query.dest)
            else
                res.redirect('/');
        }
    }
}); */

router.get('/user', function(req, res, next){
    var page = {
        layout: 'frontend/interna',
        title: 'Profilo',
        slogan: CONFIG.TITLE,
        lang: req.cookies.ev_i18n
    }

    if(!req.session.passport.user)
        res.redirect('/');
    else
        res.render('frontend/user', page);    
});

router.get(['/','/en'], function(req,res,next) {

    var lang = req.url === '/en' ? 'en' : 'it';

    i18n.setLocale(req, lang)

    logger.verbose('Richiesta la home nella lingua %s',lang)

    _getCategorieEvidenza(lang, function(err, categorieEvidenza) { 

        if(err) return next(err)

        var page = {
            lang: lang,
            layout: 'frontend/home',
            title: res.__('slogan'), // tradotto
            slogan: CONFIG.TITLE,
            description: res.__('description'), // tradotto
            url: req.path,
            prefix : req.prefix,
            langs: { it: '/', en: '/'},
            categorie: categorieEvidenza.categorie || []
        }

        res.render('frontend/home', page, function(err,html){

            if(err){
                logger.error('%j',err)
                return next(err)
            }

            res.status(200).send(html)
        });
    })
})

/* tutto il resto, statiche prodotti categorie ecc...*/

/* pagien conferma ordine, deve essere un post per uniformita con paypal */
router.post('/ordine-completato', function(req,res,next) {

    logger.verbose('Ho richiesto la pagina ordine completato')
    
    var page = {
            layout : 'frontend/interna',
            lang: 'it',
            url: req.path,
            title: 'Ordine completato!',
            slogan: CONFIG.TITLE
        };   

    res.render('frontend/ordine-completato',page, function(err, html) {

        if(err){
            logger.error('Errore durante il rendering della pagina del ordine completato %j',err)
            return next(err)
        }

        res.status(200).send(html)            
    })
})


router.get('/*', function(req,res,next) {

    i18n.setLocale(req, req.lang)

    var lang = req.lang,
        page = {
            layout : 'frontend/interna',
            lang: lang,
            url: req.path,
            baseUrl : req.baseUrl.substring(0, req.baseUrl.length - 1),
            slogan: CONFIG.TITLE
        };   


    logger.verbose('Richiesta la pagina interna %s nella lingua %s', req.path, lang)

    /* @@@@@ carrello @@@@@@ */

    if(req.path === '/carrello') {

        logger.verbose('Ho richiesto la pagina del carrello')
        
        page.title = 'Carrello';

        res.render('frontend/carrello',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina del carrello %j',err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }
    /* @@@@@ carrello-consegna @@@@@@ */

    else if(req.path === '/carrello-consegna') {

        logger.verbose('Ho richiesto la pagina della consegna')
        
        page.title = 'Consegna';

        res.render('frontend/carrello-consegna',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina della consegna %j',err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }

    /* @@@@@ carrello-pagamento @@@@@@ */

    else if(req.path === '/carrello-pagamento') {

        logger.verbose('Ho richiesto la pagina del pagamento')
        
        page.title = 'Pagamento';

        res.render('frontend/carrello-pagamento',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina del pagamento %j',err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }

    /* @@@@@ carrello-riepilogo @@@@@@ */

    else if(req.path === '/carrello-riepilogo') {

        logger.verbose('Ho richiesto la pagina del riepilogo')
        
        page.title = 'Riepilogo';
        page.cart = req.session.cart;
        page.user = req.session.passport.user;
        page.paypal = {
            url: CONFIG.PAYPAL_URL,
            logo: CONFIG.PAYPAL_LOGO,
            return: CONFIG.URL + 'ordine-completato',
            email: CONFIG.PAYPAL_MAIL,
            anagrafica: req.session.cart.fattura ? req.session.passport.user.user_metadata.fatturazione : req.session.passport.user.user_metadata
        }

        _.extend(page.paypal.anagrafica,{email: req.session.passport.user.email })

        res.render('frontend/carrello-riepilogo',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina del riepilogo %j',err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }


    /* @@@@@ cerca @@@@@@ */

    else if(req.path === '/cerca') {

        logger.verbose('Ho richiesto la pagina della ricerca')
        
        page.title = page.title = res.__('risultatiricerca');
        page.chiave = req.query.chiave;
        page.vuota = req.query.chiave === '';


        Prodotto.find({keywords:{ $all: req.query.chiave.split(' ')}, visibile: true})
            .populate('categoria')
            .limit(40)
            .exec(function(err,result){

                if (err){
                    logger.error(err)
                    next(err);
                }

                page.results = result;
                page.hasresults = result.length > 0;

                if(page.hasresults) {
                    page.results.forEach(function(r){
                        UTILS.keepCurentLang(r,lang)
                    })
                }

                res.render('frontend/risultati-ricerca',page, function(err, html) {

                    if(err){
                        logger.error('Errore durante il rendering della pagina dei risultati della ricerca %j',err)
                        return next(err)
                    }

                    res.status(200).send(html)            
                })

            })        
    }

    /* @@@@@ Chi siamo @@@@@@ */

    else if(req.path == staticRoutes.chisiamo.it || req.path == staticRoutes.chisiamo.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('chisiamo'))
        
        page.title = res.__('chisiamo');
        page.description = res.__('chisiamo_meta_description');
        page.langs = staticRoutes.chisiamo

        res.render('frontend/chi-siamo',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina %s %j',res.__('chisiamo'),err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }

    /* @@@@@ Dove siamo @@@@@@ */

    else if(req.path == staticRoutes.dovesiamo.it || req.path == staticRoutes.dovesiamo.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('dovesiamo'))
        
        page.title = res.__('dovesiamo');
        page.description = res.__('dovesiamo_meta_description');
        page.langs = staticRoutes.dovesiamo

        res.render('frontend/dove-siamo',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina %s %j',res.__('dovesiamo'),err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }

    /* @@@@@ Condizioni @@@@@@ */

    else if(req.path == staticRoutes.condizioni.it || req.path == staticRoutes.condizioni.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('condizioni'))
        
        page.title = res.__('condizioni');
        page.description = res.__('condizioni_meta_description');
        page.langs = staticRoutes.condizioni

        res.render('frontend/condizioni',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina %s %j',res.__('condizioni'),err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }



    /* @@@@@ Ecatalogue @@@@@@ */

    else if(req.path == staticRoutes.catalogue.it || req.path == staticRoutes.catalogue.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('catalogue'))
        
        page.title = res.__('catalogue');
        page.description = res.__('catalogue_meta_description');
        page.langs = staticRoutes.catalogue

        res.render('frontend/catalogue',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina %s %j',res.__('catalogue'),err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }


    /* @@@@@ Contatti @@@@@@ */

    else if(req.path == staticRoutes.contatti.it || req.path == staticRoutes.contatti.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('contatti'))
        
        page.title = res.__('contatti');
        page.description = res.__('contatti_meta_description');
        page.langs = staticRoutes.catalogue

        if(req.session.passport.user)
            page.user = req.session.passport.user;

        res.render('frontend/contatti',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina %s %j',res.__('contatti'),err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }


    /* @@@@@ Legal @@@@@@ */

    else if(req.path == staticRoutes.legal.it || req.path == staticRoutes.legal.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('legal'))
        
        page.title = res.__('legal');
        page.description = res.__('legal_meta_description');
        page.langs = staticRoutes.legal

        res.render('frontend/legal',page, function(err, html) {

            if(err){
                logger.error('Errore durante il rendering della pagina %s %j',res.__('legal'),err)
                return next(err)
            }

            res.status(200).send(html)            
        })
    }

    /* @@@@@ sitemap @@@@@@ */

    else if(req.path == staticRoutes.sitemap.it || req.path == staticRoutes.sitemap.en) {

        logger.verbose('Ho richiesto la pagina %s',res.__('sitemap'))
        
        page.title = res.__('sitemap');
        page.description = res.__('sitemap_meta_description');
        page.langs = staticRoutes.sitemap

        _getMenuCategorie(lang, function(err, categorie) {

            if(err)
                return next(err)

            page.categorie = categorie;

            res.render('frontend/sitemap',page, function(err, html) {

                if(err){
                    logger.error('Errore durante il rendering della pagina %s %j',res.__('sitemap'),err)
                    return next(err)
                }

                res.status(200).send(html)            
            })
        })

    }

    /* @@@@@ Elenco categorie @@@@@@ */

    else if(req.path == staticRoutes.categorie.it || req.path == staticRoutes.categorie.en) {

        logger.verbose('Ho richiesto la pagina delle categorie')
        
        page.title = res.__('categorie');
        page.description = res.__('categorie_meta_description');
        page.langs = staticRoutes.categorie
        page.baseUrl = req.baseUrl.substring(0, req.baseUrl.length - 1);


        _getMenuCategorie(lang, function(err, categorie) {

            console.log(_.map(categorie,'url'))

            if(err)
                return next(err)

            page.categorie = categorie;

            res.render('frontend/categorie',page, function(err, html) {

                if(err){
                    logger.error('Errore durante il rendering della pagina delle categorie %j',err)
                    return next(err)
                }

                res.status(200).send(html)            
            })
        })
    }

    /* @@@@@ Prodotto o categoria @@@@@@ */

    else if(req.alias) {

        var tpl = 'frontend/' + req.alias.type.toLowerCase(), // frontend/categoria...prodotto....
            pageNumber = req.query.page ? parseInt(req.query.page) : 0; 

        logger.verbose('La pagina %s è dinamica', req.alias.url)


        if(req.alias.type === 'Categoria') {

            logger.verbose('La pagina %s è una categoria', req.alias.url)

            _getCategoria(req.alias,lang,pageNumber, function(err, categoria) {

                if(err) {
                    logger.verbose('Errore recupero categoria, rimando alla home')
                    return res.redirect('/')
                }

                logger.verbose('Renderizzo la categoria %j', categoria._id)

                page.title = categoria.meta_title;
                page.description = categoria.meta_description;
                page.page = pageNumber;
                page.baseUrl = req.baseUrl

                _.extend(page,categoria)

                res.render(tpl,page,function(err, html){
                    if(err){
                        logger.error('Errore durante il rendering della pagina %s %j',req.alias.url,err)
                        return next(err)
                    }

                    res.status(200).send(html)   
                })
            })
        }
        if(req.alias.type === 'Prodotto') {

            logger.verbose('La pagina %s è un prodotto', req.alias.url)

            _getProdotto(req.alias, lang, function(err, prodotto) {

                if(err) {
                    logger.verbose('Errore recupero prodotto, rimando alla home')
                    return res.redirect('/')
                }

               logger.verbose('Renderizzo il prodotto %j', prodotto._id)

               if(prodotto.varianti && prodotto.varianti[0]) {
                if(_.uniq(_.map(prodotto.varianti[0].opzioni,'prezzo')).length > 1)
                    prodotto.apartire = true
               }

               var _metaFallback = function () {
                    var d = res.__('categoria') +': '+ prodotto.categoria.nome;
                    d += ', ' +res.__('articolo') +': ' + prodotto.nome + ' - '+prodotto.descrizione;
                    d += ', '+ res.__('prezzo')+': '+ (!prodotto.apartire ? UTILS.euro(prodotto.prezzo) : res.__('partire') +': '+ UTILS.euro(prodotto.prezzo)); 
                    return d;
               }

                page.title = prodotto.meta_title;
                page.description = prodotto.meta_description || _metaFallback();
                page.data = prodotto;
                page.baseUrl = req.baseUrl

                _.extend(page,prodotto)

                res.render(tpl,page,function(err, html){

                    if(err){
                        logger.error('Errore durante il rendering della pagina %s %j',req.alias.url,err)
                        return next(err)
                    }

                    res.status(200).send(html)   
                })

            })
        }

        
    }

    else {
        logger.warn('404: La pagina %s non esiste',req.path)
        UTILS.page404(req,res,next);
    }


})


module.exports = router
