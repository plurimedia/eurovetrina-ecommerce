
var express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    CONFIG = require(process.cwd() + '/config'),
	UTILS = require(process.cwd() + '/server/utils'),
    Categoria = mongoose.model('Categoria'),
    Prodotto = mongoose.model('Prodotto'),
    Ordine = mongoose.model('Ordine'),
	logger = require(process.cwd() + '/server/logger'),
	router = express.Router(),
    page = {
        layout: 'backend/default'
    };

// admin/dashboard
router.get('/dashboard', function (req, res, next) {


    page.url = req.url
    page.count = {}

    async.parallel([
        function(callback) {
            Categoria.count({}, function( err, count){

                if (err)
                    return callback(err);

                page.count.categorie = count;
                callback()
              
            })            
        },
        function(callback) {

            Prodotto.count({}, function( err, count){

                if (err)
                    return callback(err);

                page.count.prodotti = count;
                callback()
              
            })  
        },
        function(callback) {

            Ordine.count({}, function( err, count){

                if (err)
                    return callback(err);

                page.count.ordini = count;
                callback()
              
            })  
        },
        function(callback) {

            Ordine.find({stato: {$gte:2}}, function(err, ordini){
                if(err)
                    return callback(err)

                var totale = ordini.reduce(function(a, b){
                            return a + b.carrello.totale;
                        }, 0);


                page.totaleordini = totale;
                callback()

            }).lean()
        }
    ],
    // optional callback
    function(err, results) {

        if(err)
            return next(err)

        res.render('backend/dashboard', page)         

    });    

 
});


// admin/categorie (elenco)
router.get('/categorie', function (req, res, next) {

    page.url = req.url;
    res.render('backend/categorie', page)   
});

// admin/categorie/evidenza (elenco)
router.get('/categorie/evidenza', function (req, res, next) {
    page.url = req.url;
    res.render('backend/categorie-evidenza', page)   
});


// admin/categoria (dettaglio o nuova)
router.get('/categoria/:id?', function (req, res, next) {

    page.url = req.url;
    res.render('backend/categoria', page)   
});

// admin/categoria/{id}/ordina (gestione ordine prodotti nella categoria)
router.get('/categoria/:id/ordina', function (req, res, next) {

    page.url = req.url;
    res.render('backend/categoria-ordina', page)   
});

// admin/categoria/{id}/alias (mostra gli alias della categoria)
router.get('/categoria/:id/alias', function (req, res, next) {

    page.url = req.url;
    res.render('backend/categoria-alias', page)   
});


// admin/prodotti (elenco)
router.get('/prodotti', function (req, res, next) {

    page.url = req.url;
    res.render('backend/prodotti', page)   
});

// admin/prodotto (dettaglio o nuovo)
router.get('/prodotto/:id?', function (req, res, next) {

     page.url = req.url;
    res.render('backend/prodotto', page)   
});

// admin/prodotto/{id}/alias (gestione ordine prodotti nella categoria)
router.get('/prodotto/:id/alias', function (req, res, next) {

    page.url = req.url;
    res.render('backend/prodotto-alias', page)   
});

// admin/ordini (elenco)
router.get('/ordini', function (req, res, next) {

    page.url = req.url;
    res.render('backend/ordini', page)   
});

// admin/ordine (dettaglio)
router.get('/ordine/:id', function (req, res, next) {
    page.url = req.url;
    res.render('backend/ordine', page)   
});


module.exports = router