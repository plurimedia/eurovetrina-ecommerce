var mongoose = require('mongoose'),
    async = require('async'),
    slug = require('slugger'),
    _ = require('lodash'),
    logger = require(process.cwd() + '/server/logger'),
    Alias = mongoose.model('Alias'),
    MenuCategorie = mongoose.model('MenuCategorie'),
    Schema = mongoose.Schema,
    CategoriaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        immagine: {
            type: String         
        },
        it: { // oggetto mixed con tutti i campi italiani
            type: Schema.Types.Mixed,
            required: true
        },
        en: { // oggetto mixed con tutti i campi inglesi
            type: Schema.Types.Mixed
        }
    }),
    _insertAlias = function (cat, callbackGlobale) {

        var slugIT = slug(cat.it.nome),
            aliases = [{lang:'it',url:slugIT}];

        cat.it.url = slugIT;    

        if(cat.it.redirect)
            aliases.push({lang:'it',url:cat.it.redirect, redirect: slugIT })

        if(cat.en) {
            var slugEN = 'en/'+slug(cat.en.nome);
            cat.en.url = slugEN;
            aliases.push({lang:'en',url:slugEN})
            if(cat.en.redirect)
                aliases.push({lang:'en',url:'en/'+cat.en.redirect, redirect: slugEN })
        }

        // metto anche i meta title uguali al nome se non compilati
        if(!cat.it.meta_title)
            cat.it.meta_title = cat.it.nome;

        if(cat.en && !cat.en.meta_title)
            cat.en.meta_title = cat.en.nome;

        logger.verbose('Middleware categoria, salvo gli alias %j',aliases)

        var added = [];

        async.each(aliases, function(a,cbSingoloAlias) {

            a.document = cat._id;
            a.type = 'Categoria';

            Alias.findOne({url:a.url}, function(err, result){

                if (err)
                    return next(err);

                if(result) {
                    logger.verbose('Alias già presente %j, non devo salvare',a.url)
                    cbSingoloAlias(); // c'è già
                }

                else { 

                    logger.verbose('Inserisco il nuovo alias %j',a.url)

                    var alias = new Alias(a);

                    alias.save(function (err, result) {

                        if (err)
                            cbSingoloAlias(err);

                        logger.verbose('Alias salvato nel db')

                        added.push(result)
                        
                        cbSingoloAlias()
                    });                      
                    
                }

            })

        },function(err){

            logger.verbose('Controllo quali alias aggiornare a redirect')

            if(err)
                return callbackGlobale(err);


            if(added.length === 0) {
                logger.verbose('Non ho aggiunto alias, non devo imposare i redirect')
                callbackGlobale();
            }

            else {
                logger.verbose('Ho aggiunto alias nuovi, controllo il tipo')

                async.each(added, function(aggiunto,cb){

                    if(!aggiunto.redirect){

                        Alias.update({document: aggiunto.document, _id:{$ne: aggiunto._id},lang:aggiunto.lang},{$set:{redirect:aggiunto.url}},{multi: true}, function(e,r){
                            logger.verbose('Impostato nuovo redirect')
                            cb();
                        })                        
                    }
                    else {
                        logger.verbose('Non devo impostare un redirect')
                        cb();
                    }


                }, function(err){
                    callbackGlobale(); 
                })
                               
            }
            
        })

    },
    _updateMenu = function (cat, callback) {

        
        MenuCategorie.findOne({}, function(err, menu) {


            if(err){
                return next(err)
            }

            if(menu) {

                logger.verbose('Controllo se devo aggiornare il menu delle categorie')

                function cercaVoce (menu) {

                    menu.voci.forEach(function(voce) {

                        if(voce.categoria._id.toString() == cat._id.toString()) {
                            logger.verbose('Categoria presente nel menu, aggiorno %j', cat.it.nome)
                            voce.categoria = cat;
                        }
                        else {
                            if(voce.voci && voce.voci.length)
                              cercaVoce(voce)
                        }
                    });
                } 

                cercaVoce(menu); 

                MenuCategorie.findOneAndUpdate({}, menu ,function(err, resp){

                    if(err)
                        return callback(err)

                    callback()
                }) 

            }
            
            else {
                logger.verbose('Non ho menu da aggiornare')
                callback();
            }

        }).lean()
    },
    _addToMenu = function (cat, callback) {

        MenuCategorie.findOne({}, function(err, menu) {


            if(err)
                return next(err)

            var nuovaVoce = {
               categoria: cat,
               visibile: false,
               voci: []                    
            }


            if(!menu) { // prima volta, non c'è ancora il menu, lo creo

                logger.verbose('Creo il menu per la prima volta')

                var menu = { mdate: new Date(), voci: [nuovaVoce]},
                    m = new MenuCategorie(menu)
                
                m.save(function (err, result) {
                    if (err)
                        return callback(err);
                    else
                        callback()
                });               
            }

            else {

                logger.verbose('Aggiungo la nuova categoria in coda al menu')

                menu.voci.push(nuovaVoce)

                MenuCategorie.findOneAndUpdate({}, menu ,function(err, resp){

                    if(err)
                        return callback(err)

                    callback()
                }) 
            }

        }).lean()

    };

CategoriaSchema.pre('update', function(next) {

    var cat = this._update;

    async.parallel([
        function(callback) {
            _insertAlias(cat,callback)
        },
        function(callback) {
            _updateMenu(cat,callback) // dopo l'aggiornamento controllo se la categoria è in un menu, in quel caso la aggiorno
        }
    ],
    function(err, results) {
        if(err)
            return next(err)

        logger.verbose('Completato il middleware di aggiornamento categoria')
        next()
    });
})


CategoriaSchema.pre('save', function(next) {

    var cat = this;

    async.parallel([
        function(callback) {
            _insertAlias(cat,callback)
        },
        function(callback) {
            _addToMenu(cat,callback) // aggiungo in coda la nuova voce non visibile
        }
    ],
    function(err, results) {
        if(err)
            return next(err)

        logger.verbose('Completato il middleware di salvataggio nuova categoria')
        next()
    });
})


mongoose.model('Categoria', CategoriaSchema, 'Categoria');


