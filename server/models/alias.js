var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    AliasSchema = new Schema({
        lang : {
        	type: String,
        	required: true
        },
        url : {
            type: String,
            required: true,
            unique: true
        },
        redirect: {
            type: String
        },
        type: { // tipo di oggetto
            type: String,
            index:true,
            required: true
        },
        document: { // l'oggetto che deve rispondere a questo alias
            type: String,
            index:true,
            required: true
        }
    });

mongoose.model('Alias', AliasSchema, 'Alias');