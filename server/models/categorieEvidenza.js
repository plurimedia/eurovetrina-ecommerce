var mongoose = require('mongoose'),
	async = require('async'),
	_ = require('lodash'),
    Schema = mongoose.Schema,
    logger = require(process.cwd() + '/server/logger'),
    Prodotto = mongoose.model('Prodotto'),
    Categoria = mongoose.model('Categoria'),
    CategorieEvidenzaSchema = new Schema({
        categorie: [Schema.Types.Mixed]
    }),
    _aggiorna = function (evidenza, next) { // prendo le categorie aggiornate e i primi 5 prodotti

    	logger.verbose('Middleware categorie in evidenza, aggiorno l\'albero');

    	async.each(evidenza.categorie, function(e, cbPadre){

    		logger.verbose('Aggiorno %s', e.it.nome);

    		Categoria.findOne({_id: e._id}, function(err,cat){
    			if(err)
    				cbPadre(err)

    			e = _.extend(e,cat);

    	
    			// ora i figli
    			if(e.figlie && e.figlie.length) {

    				logger.verbose('%s ha figli: Prendo le categorie figlie aggiornate',e.it.nome);

    				async.each(e.figlie,function(f,cbFiglio){

    					logger.verbose('aggiorno il figlio %s',f.it.nome)
    					Categoria.findOne({_id:f._id}, function(err, cat){

			    			if(err)
			    				cbFiglio(err)

			    			f = _.extend(f,cat);

			    			logger.verbose('Prendo i prodotti di %s', f.it.nome);
			    			// cerco i prodotti 
		    				Prodotto.find({categoria:f._id}, function(err, prodotti){
				    			if(err)
				    				cbFiglio(err)  

				    			f.prodotti = prodotti;

				    			logger.verbose('Finito %s', e.it.nome);
				    			cbFiglio();
		    				})
		    				.limit(5)
		    				.sort({peso: 1})
		    				.lean()			    			

    					}).lean()

    				}, function(err){
    					if(err)
    						cbPadre(err)

    					logger.verbose('Finito %s',e.it.nome);
    					cbPadre()
    				})
    			}
    			else {
    				// cerco i prodotti
    				
    				logger.verbose('%s non ha figli, attacco solo i prodotti', e.it.nome);


    				Prodotto.find({categoria:e._id}, function(err, prodotti){
		    			if(err)
		    				cbPadre(err)  

		    			e.prodotti = prodotti;
		    			logger.verbose('Finito %s', e.it.nome);
		    			cbPadre();	
    				})
    				.limit(5)
    				.sort({peso: 1})	
    				.lean()		    			

    			}
    		}).lean()
    	},function(err){
    		if(err)
    			return next(err)
    		
    		logger.verbose('Finito il middleware delle categorie in evidenza');

    		console.log(evidenza)
    		next();
    	})
    };




CategorieEvidenzaSchema.pre('update', function(next) {

    var evidenza = this._update;
    _aggiorna(evidenza,next)
})


CategorieEvidenzaSchema.pre('save', function(next) {

    var evidenza = this;
    _aggiorna(evidenza,next)

})


mongoose.model('CategorieEvidenza', CategorieEvidenzaSchema, 'CategorieEvidenza');
