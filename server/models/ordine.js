var _ = require('lodash'),
    mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
	logger = require(process.cwd() + '/server/logger'),
    Schema = mongoose.Schema,
    OrdineSchema = new Schema({
        data : {
            type: Date,
            default: Date.now,
            index: true
        },
        keywords: [{  // ricerche
            type: String
        }],
        stato : {
            type: Number, // 1 = in attesa di pagamento, 2 = pagato, 3 = spedito, -1 = errore paypal, -2 = paypal: totali non corrispondono o stato errato
            default: 1,
            index: true
        },
        mail_cliente : { 
            type: Schema.Types.Mixed
        },
        mail_commerciante : {
            type: Schema.Types.Mixed
        },
        carrello: {
            type: Schema.Types.Mixed // tutto
        },
        utente: {
            type: Schema.Types.Mixed // tutto
        },
        paypal: {
            type: Schema.Types.Mixed // tutto
        }
    }),
    _insertKeywords = function (ordine) {

        var keywords = [];

    	try {

            keywords.push(ordine.numero.toString())
            keywords.push(ordine.utente.email)
            keywords.push(ordine.utente.user_metadata.nome)
            keywords.push(ordine.utente.user_metadata.nome.toLowerCase())
            keywords.push(ordine.utente.user_metadata.cognome)
            keywords.push(ordine.utente.user_metadata.cognome.toLowerCase())
            keywords.push(ordine.utente.user_metadata.cf)

            if(ordine.utente.user_metadata.fatturazione) {
                keywords.push(ordine.utente.user_metadata.fatturazione.iva)
                keywords.push(ordine.utente.user_metadata.fatturazione.nome)
                keywords.push(ordine.utente.user_metadata.fatturazione.nome.toLowerCase())
                keywords.push(ordine.utente.user_metadata.fatturazione.cf)
            }

    		return _.uniq(keywords);

    	}
    	catch(err) {
    		logger.error('Errore durante il salvataggio delle keywords ordine %j', err)
    		return [];
    	}
    };

OrdineSchema.index({keywords: 1});
OrdineSchema.index({numero: 1});
OrdineSchema.index({stato: 1});
autoIncrement.initialize(mongoose.connection);
OrdineSchema.plugin(autoIncrement.plugin, { model: 'Ordine', field: 'numero', startAt: 12340 });


OrdineSchema.pre('save', function(next) {

    var ordine = this;
    ordine.keywords = _insertKeywords(ordine)
    next();

})

OrdineSchema.pre('update', function(next) {

    var ordine = this._update;
    ordine.keywords = _insertKeywords(ordine)
    next();
})


mongoose.model('Ordine', OrdineSchema, 'Ordine');