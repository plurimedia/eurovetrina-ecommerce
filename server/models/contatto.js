var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ContattoSchema = new Schema({
        data : {
            type: Date,
            default: Date.now,
            index: true
        },
        nome : {
            type: String,
            required: true
        },
        cognome : {
            type: String,
            required: true
        },
        email : {
            type: String,
            required: true
        },
        telefono : {
            type: String,
            required: true
        },
        messaggio : {
            type: String,
            required: true
        }
    });

mongoose.model('Contatto', ContattoSchema, 'Contatto');