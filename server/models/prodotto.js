var mongoose = require('mongoose'),
    async = require('async'),
    _ = require('lodash'),
    slug = require('slugger'),
    logger = require(process.cwd() + '/server/logger'),
    AliasSchema = require(process.cwd() + '/server/models/alias'),
    Alias = mongoose.model('Alias'),
    Schema = mongoose.Schema,
    ProdottoSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now,
            index: true
        },
        visibile: { // si/no
            type: Boolean,
            default: false,
            index: true
        },
        codice: {
            type: String,
            required: true,
            unique: true
        },
        categoria: {
            type: Schema.ObjectId, // ref a categoria
            ref: 'Categoria',
            required: true
        },
        peso: { // per ordinarlo nella sua categoria
            type: Number,
            default: 0,
            index: true
        },
        keywords: [{  // ricerche (nome categoria, nome it, nome en ...)
            type: String
        }],
        immagine: {
            type: String         
        },
        gallery: {
            type: Schema.Types.Mixed,      
        },
        it: { // oggetto mixed con tutti i campi italiani
            type: Schema.Types.Mixed,
            required: true
        },
        en: { // oggetto mixed con tutti i campi inglesi
            type: Schema.Types.Mixed
        }
    }),
    _insertAlias = function (prod, callbackGlobale) {


        var slugIT = slug(prod.it.nome+'-'+prod.codice),
            aliases = [{lang:'it',url:slugIT}];

        prod.it.url = slugIT;

        if(prod.it.redirect)
            aliases.push({lang:'it',url: prod.it.redirect, redirect: slugIT })

        if(prod.en) {
            var slugEN = 'en/'+slug(prod.en.nome+'-'+prod.codice);
            prod.en.url = slugEN;
            aliases.push({lang:'en',url:slugEN})
            if(prod.en.redirect)
                aliases.push({lang:'en',url:'en/'+prod.en.redirect, redirect: slugEN })
        }

        // metto anche i meta title uguali al nome se non compilati
        if(!prod.it.meta_title)
            prod.it.meta_title = prod.it.nome;

        if(prod.en && !prod.en.meta_title)
            prod.en.meta_title = prod.en.nome;


        logger.verbose('Middleware prodotto, salvo gli alias %j',aliases)

        var added = [];

        async.each(aliases, function(a,cbSingoloAlias) {

            a.document = prod._id;
            a.type = 'Prodotto';

            Alias.findOne({url:a.url}, function(err, result){

                if (err)
                    return next(err);

                if(result) {
                    

                    if(result.document != prod._id) {
                        logger.warn('Ho inserito lo stesso alias in un prodotto diverso, non lo salvo')
                        delete prod[result.lang].redirect;
                    }
                    else {
                        logger.verbose('Alias già presente %j, non devo salvare',a.url)

                    }

                    cbSingoloAlias(); // c'è già
                }

                else { 

                    logger.verbose('Inserisco il nuovo alias %j',a.url)

                    var alias = new Alias(a);

                    alias.save(function (err, result) {

                        if (err)
                            cbSingoloAlias(err);

                        logger.verbose('Alias salvato nel db')

                        added.push(result)
                        
                        cbSingoloAlias()
                    });                      
                    
                }

            })

        },function(err){

            logger.verbose('Controllo quali alias aggiornare a redirect')

            if(err)
                return callbackGlobale(err);


            if(added.length === 0) {
                logger.verbose('Non ho aggiunto alias, non devo imposare i redirect')
                callbackGlobale();
            }

            else {
                logger.verbose('Ho aggiunto alias nuovi, controllo il tipo')

                async.each(added, function(aggiunto,cb){

                    if(!aggiunto.redirect) {

                        prod[aggiunto.lang].url = aggiunto.url

                        Alias.update({document: aggiunto.document, _id:{$ne: aggiunto._id},lang:aggiunto.lang},{$set:{redirect:aggiunto.url}},{multi: true}, function(e,r){
                            logger.verbose('Impostato nuovo redirect')
                            cb();
                        })                        
                    }
                    else {
                        logger.verbose('Non devo impostare un redirect')
                        cb();
                    }


                }, function(err){
                    callbackGlobale(); 
                })
                               
            }
            
        })

    },
    _insertKeywords = function (prod, callback) {

        try {

            var nomeit = prod.it.nome.split(' '), // parole nome prodotto
                keywords = [nomeit,prod.codice, prod.codice.toLowerCase()]; // minimo ho queste

            if(prod.en && prod.en.nome)// se ho anche la parte inglese..
                keywords.push(prod.en.nome.split(' '))

            if(prod.codice.indexOf('/')!==-1) { // tutte le parti tra / del codice di almeno due caratteri
                prod.codice.split('/').forEach(function(p){
                    if(p.length >=2)
                        keywords.push(p)
                    if(p.indexOf(' ')!==-1) { // se a sua volta le parti contengono spazi...
                        p.split(' ').forEach(function(p){
                            if(p.length >=2)
                                keywords.push(p)
                        })
                    }
                })
            }

            if(prod.codice.indexOf('-')!==-1) { // tutte le parti tra - del codice di almeno due caratteri
                prod.codice.split('-').forEach(function(p){
                    if(p.length >=2)
                        keywords.push(p)
                })
            }

            if(prod.codice.indexOf(' ')!==-1) { // tutte le parte tra "spazio" del codice di almeno due caratteri
                prod.codice.split(' ').forEach(function(p){
                    if(p.length >=2)
                        keywords.push(p)
                })
            }

            if(prod.codice.indexOf('+')!==-1) { // tutte le parte tra "+" del codice di almeno due caratteri
                prod.codice.split('+').forEach(function(p){
                    if(p.length >=2)
                        keywords.push(p)
                })
            }

            keywords = _.flatten(keywords);

            // ci metto anche tutti i lowercase
            keywords.forEach(function(k){
                keywords.push(k.toLowerCase())
            })     

            // ci metto anche tutti gli uppercase
            keywords.forEach(function(k){
                keywords.push(k.toUpperCase())
            })  

            keywords = _.uniq(keywords)

            logger.verbose('Nuove keywords per il prodotto %s: %j', prod.it.nome, keywords) 

            prod.keywords = keywords;

            callback()    
        }
        catch(err) {
            logger.error('Errore durante la generazione delle keywords di prodotto %j', err)
            callback(err)
        }
    };

ProdottoSchema.index({keywords: 1, categoria: 1});


ProdottoSchema.pre('update', function(next) {

    var prod = this._update;

    async.parallel([
        function(callback) {
            _insertAlias(prod,callback)
        },
        function(callback) {
            _insertKeywords(prod,callback)
        }
    ],
    function(err, results) {
        if(err)
            return next(err)

        logger.verbose('Completato il middleware di aggiornamento prodotto')
        next()
    });
})



ProdottoSchema.pre('save', function(next) {

    var prod = this;

    async.parallel([
        function(callback) {
            _insertAlias(prod,callback)
        },
        function(callback) {
            _insertKeywords(prod,callback)
        }
    ],
    function(err, results) {
        if(err)
            return next(err)

        logger.verbose('Completato il middleware di salvataggio prodotto')
        next()
    });
})


mongoose.model('Prodotto', ProdottoSchema, 'Prodotto');




