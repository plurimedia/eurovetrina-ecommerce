var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    MenuCategorieSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        voci: [Schema.Types.Mixed]
    });

mongoose.model('MenuCategorie', MenuCategorieSchema, 'MenuCategorie');