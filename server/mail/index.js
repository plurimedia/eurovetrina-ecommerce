var CONFIG = require(process.cwd() + '/config.js'),
    UTILS = require(process.cwd() + '/server/utils'),
    logger = require(process.cwd() + '/server/logger'),
    mandrill = require('mandrill-api/mandrill'),
    mandrill_client = new mandrill.Mandrill(CONFIG.MANDRILL_API_KEY),
    juice = require('juice'), // mette gli stili in linea in un html
    fs = require('fs'),
    Handlebars = require('handlebars'),
    helpers = require('handlebars-helpers')({
        handlebars: Handlebars
    }),
    _ = require('lodash'),
    Tcss = fs.readFileSync(process.cwd() + '/server/mail/style.css', 'utf8'),
    Theader = fs.readFileSync(process.cwd() + '/server/mail/header.html', 'utf8'), // header comune a tutte le mail
    Tfooter = fs.readFileSync(process.cwd() + '/server/mail/footer.html', 'utf8'), // footer comune a tutte le mail
    TVerificaUtente = fs.readFileSync(process.cwd() + '/server/mail/verifica-utente.html', 'utf8'),
    TRiepilogoOrdine = fs.readFileSync(process.cwd() + '/server/mail/ordine-riepilogo.html', 'utf8'),
    TOrdineCliente = fs.readFileSync(process.cwd() + '/server/mail/ordine-cliente.html', 'utf8'),
    TOrdineCommerciante = fs.readFileSync(process.cwd() + '/server/mail/ordine-commerciante.html', 'utf8'),
    TContatto = fs.readFileSync(process.cwd() + '/server/mail/contatto.html', 'utf8'),
    // sistema i dentinatari col formato di mandrill
    _to = function (to,cc,ccn) {
        var toForMandrill = [],
        _formattaIndirizzi = function (indirizzi, tipo) {
            return _.map(indirizzi, function (ind) {
                return {
                    email: ind,
                    type: tipo
                }
            })
        };
        // mandrill li vuole in questo formato
        if (to)
            toForMandrill = _.union(toForMandrill, _formattaIndirizzi(to, 'to'));
        if (cc)
            toForMandrill = _.union(toForMandrill, _formattaIndirizzi(cc, 'cc'));
        if (ccn)
            toForMandrill = _.union(toForMandrill, _formattaIndirizzi(ccn, 'bcc'));

        return toForMandrill;
    };


/*
*   type: {String} tipo di mail che devo inviare, sceglie il template
*   data: {Object} dati da usare nel template
*   subject: {String} oggetto della mail
*   from: {Object} mittente della mail ({name:'', email:''})
*   to: {String} destinatario della mail
*   cc: {String} cc della mail
*   ccn: {String} ccn della mail
*   reply_to {String} eventuale reply to 
 */

exports.send = function (type,data,subject,from,to,cc,ccn,callback) {

    try {
        // scegli i template giusti
        switch(type){

            case 'ordine-cliente': 
            Tcontent = TOrdineCliente;
            break;

            case 'ordine-commerciante': 
            Tcontent = TOrdineCommerciante;
            break;   

            case 'verifica-utente': 
            Tcontent = TVerificaUtente;
            break;    

            case 'contatto': 
            Tcontent = TContatto;
            break;           

            default: 
            return; // fermati
        }

        Handlebars.registerPartial('header',Theader);
        Handlebars.registerPartial('footer',Tfooter);
        Handlebars.registerPartial('css',Tcss);
        Handlebars.registerPartial('riepilogo-ordine',TRiepilogoOrdine);

        Handlebars.registerHelper('euro', function(value){
            return UTILS.euro(value);
        });
        Handlebars.registerHelper('cloudinary', function (id,transformation) { 
            return UTILS.cloudinaryUrl(id,transformation)
        })

        console.log(data)

        var compiledTemplate = Handlebars.compile(Tcontent),
            message = { // preparo il messaggio
                html: juice(compiledTemplate(_.extend(data, {app_url: CONFIG.URL}))),
                subject: subject,
                from_email: CONFIG.EMAIL_FROM,
                from_name: CONFIG.EMAIL_FROM_NAME,
                to: _to(to,cc,ccn), // come li vuole mandrill
                headers: {
                    "Reply-To": CONFIG.EMAIL_FROM
                },
                track_opens: 1,
                track_clicks: 1
            };

        // mando!
        mandrill_client.messages.send({
                message: message,
                async: false, // singola mail non serve un batch
            },
            function (result) {
                logger.info('Mail spedita', result)
            	callback(null,result)
            },
            function (e) {
            	logger.error('Impossibile inviare la mail %s', e)
            	callback(e)
            }
        );
    }
    catch(err) {
        console.log('impossibile inviare la mail', err)
    }
}

exports.stato = function (req, res, next) {
    mandrill_client.messages.info({
            id: req.params.id
        },
        function (result) {
            res.status(200).send(result)
        },
        function (err) {
            return next(err)
        }
    )
}

