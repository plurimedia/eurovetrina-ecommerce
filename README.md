# Ecommerce eurovetrina

## Installazione

* npm install
* nodemon

* node -r dotenv/config index.js

## far partire l'app in locale

* procurarsi il file .env
* npm install
* npm install -g nodemon
* nodemon

## per pubblicare su heroku
git push heroku-test master -> per mettere il branch di master sull'ambiente di test
git push heroku-test test -> per mettere il branch di test sull'ambiente di test
git push heroku master -> per mettere il branch di master sull'ambiente di prod

## Ambienti

Il sistema cerca la variabile di env "ambiente"

* ambiente = prod, Produzione
* ambiente = test, Test
* ambiente = undefined , Dev

### Test

Usa una sandbox di mlab e il free plan di heroku

* https://eurovetrina-ecommerce-test.herokuapp.com/
* mongo ds145780.mlab.com:45780/heroku_dzft4j37 -u plurimedia -p Canonico27!
* deploy: git push heroku-test {branch}
* logs: heroku logs --app eurovetrina-ecommerce-test
* importare db dump in dev: mongorestore -h localhost:27017 -d eurovetrina_ecommerce ./dump/heroku_gw49016w
* importare db dump in test: mongorestore -h ds145780.mlab.com:45780 -d heroku_dzft4j37 -u plurimedia -p Canonico27! ./dump/heroku_gw49016w

### Produzione

* https://eurovetrina-ecommerce-prod.herokuapp.com/
* mongo ds153610-a0.mlab.com:53610/heroku_gw49016w -u eurovetrina -p V=y7tyH8\;J8^XGU
* deploy: git push heroku-prod {branch}
* logs:  heroku logs --app eurovetrina-ecommerce-prod --tail
* export db: mongodump -h ds153610-a0.mlab.com:53610 -d heroku_gw49016w -u eurovetrina -p V=y7tyH8\;J8^XGU -o ~/Documents/projects/git/eurovetrina-ecommerce/dump

## Struttura dati

La gestione di categorie, prodotti si basa su 4 collection

* Categoria
* Prodotto
* Alias
* MenuCategorie

Ogni prodotto ha un ref alla sua categoria. Prodotti e categorie salvano i loro url nella tabella alias.
Ogni alias contiene, l'id del documento da recuperare, la lingua e la stringa dell'url.
L'url viene calcolato automaticamente in base al nome del prodotto

Nei middleware di Categoria e prodotto ogni volta che salvo inserisco un alias.
Se cambiano di nome, salvo l'alias e imposto a redirect quelli vecchi.

L'utente può aggiungere manualmente un redirect per quella lingua in una delle due entità.
Se inserisce lo stesso alias in due prodotti diversi il sistema non salva quello inserito per ultimo.
Nell'alias di una lingua diverso dall'italiano viene appeso il prefisso (es: en/name-of-product)

La collection MenuCatagorie costruisce il menu gerarchico delle categorie. Un prodotto può essere associato solo alle categorie figlie, o alla categoria di primo livello che non ha figli. Nell'albero specifico tramite la proprietà "visibile" se mostrare o meno la categoria nel menu.

Il prodotto ha anche lui una proprietà visibile che ne condiziona la visibilità sul frontend.


## Multilingua

Ogni documento a due proprietà it, en che contengono i campi delle due lingue.
In fase di rendering del frontend tengo (con un parsing) solo la proprietà della lingua corrente in request.


## paypal developer account

matteo.leoni@plurimedia.it/pluri9900


## paypal test buyer

webmktg@plurimedia.it/pluri9900

carta di prova VISA
4556180572054773 codice 123
08/20

## paypal test seller

plurimedia.web@plurimedia.it/pluri9900


## Paypal produzione

info@eurovetrinaespositori.it
E260/on!

## Casella gmail

info@eurovetrinaespositori.it
E260/on!

## admin

prod: roberta.levito@plurimedia.it/Canonico27!
dev/test: matteo.leoni@plurimedia.it/pluri9900
dev/test: matteo.cozzoli@plurimedia.it/pluri9900
26/01/2022 : in test su heroku mi funziona solo luca.sammali@plurimedia.it/password

## redattore

prod e test: eurovetrina@tiscali.it/Ev@Virg17!

## domini configurati su heroku

www.eurovetrinaespositori.it -> www.eurovetrinaespositori.it.herokudns.com 
www.eurovetrina.it -> www.eurovetrina.it.herokudns.com
www.eurovetrina.com -> www.eurovetrinaespositori.com.herokudns.com

per vedere i domini: heroku domains --app eurovetrina-ecommerce-prod

## account register per i domini

USERID           PG9523-EURO
PASSWORD         jMj8weZBQNmT

## CONFIGURAZIONE AUTH0

ci vogliono due applicazioni su auth0, una machine to machine per fare le operazioni sugli utenti (create, update) e una regular web application per le normali operazioni di login. Per ora abbiamo solo una applicazione M2M chiamata eurovetrina (Test Client)
