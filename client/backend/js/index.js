numeral.locale('it');

var backend = angular.module('backend', [
        'bootcomplete', // autocomplete
        'ngResource', // services
        'ngNotify', // messagggi
        'mgcrea.ngStrap', // angular strap
        'ngAnimate', // animate
        'ngFileUpload', // upload files
        'angular-sortable-view', // ordinamento con drag & drop
        'ui.tree' // tree sortable
    ])
    .config(function($datepickerProvider, $modalProvider, $interpolateProvider) {
        $interpolateProvider.startSymbol('[[').endSymbol(']]');

        angular.extend($datepickerProvider.defaults, {
            dateFormat: 'dd/MM/yyyy',
            startWeek: 1
        });

        angular.extend($modalProvider.defaults, {
            animation: 'am-slide-top'
        });
    })
    .directive('cloudinary', function() { // stampa il tag img di una foto su cloudinary
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                transformation: '@',
                bootstrapclass: '@'
            },
            template: '<img ng-src="https://res.cloudinary.com/plurimedia/image/upload/{{transformation}}/{{id}}" class="{{bootstrapclass}}"/>'
        }
    })
    .directive('statoOrdine', function () { // stampa lo stato di un ordine
        return {
            restrict: 'E',
            replace: true,
            scope: {
                stato: '@',
            },
            template: '<span><span ng-if="stato===\'1\'" class="label label-warning">In attesa di pagamento</span>\
                       <span ng-if="stato===\'2\'" class="label label-info">Pagato</span>\
                       <span ng-if="stato===\'3\'" class="label label-primary">Spedito</span>\
                       <span ng-if="stato===\'-1\'" class="label label-danger">Errore paypal</span>\
                       <span ng-if="stato===\'-2\'" class="label label-danger">Pagamento non accettato</span>\
                       </span> '
        }
    })
    .directive('statoMail', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                mail: "="
            },
            template: '<span>\
                        <span ng-if="mail.state==\'sent\'" class="label label-default"><i class="fa fa-paper-plane"></i> INVIATA <span ng-if="mail.opens > 0"> E LETTA</span></span>\
                        <span ng-if="mail.state==\'soft-bounced\' || stato==\'hard-bounced\' || stato==\'bounced\'" class="label label-danger"><i class="fa fa-hand-stop-o"></i> RESPINTA</span>\
                        <span ng-if="mail.state==\'spam\'" class="label label-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> SPAM</span>\
                        <span ng-if="!mail.state" class="label label-default"><i class="fa fa-clock-o" aria-hidden="true"></i> Dati non disponibili</span>\
                        </span>'
        }
    })
    .filter('euro', function () {
        return function (input) {
            if(input === 0)
                return numeral(0).format('$0,0.00')
            else
                return input ? numeral(input).format('$0,0.00') : '';
        }
    })
    .service('ALIAS', function($resource) {
        return $resource('/api/secure/alias/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        })
    })
    .service('CATEGORIA', function($resource) {
        return $resource('/api/secure/categoria/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        })
    })
    .service('PRODOTTO', function($resource) {
        return $resource('/api/secure/prodotto/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        })
    })
    .service('ORDINE', function($resource) {
        return $resource('/api/secure/ordine/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        })
    })
    .service('EMAIL', function ($resource) {
        return $resource('/api/secure/email/:id',{id: '@id'},{
            'status': {method: 'GET'}
        });
    })
    .service('MENUCATEGORIE', function($resource) {
        return $resource('/api/secure/menucategorie/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        })
    })
    .service('CATEGORIEVIDENZA', function($resource) {
        return $resource('/api/secure/categorievidenza/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        })
    })
    .service('UTILITIES', function($location) {

        var utilities = {};
        // chiave per upload unsigned su cloudinary, cambia da sviluppo a produzione

        utilities.cloudinary_upload_preset = 'eurovetrina-ecomm-prod';

        if ($location.absUrl().indexOf('localhost') !== -1 || $location.absUrl().indexOf('test') !== -1)
            utilities.cloudinary_upload_preset = 'eurovetrina-ecomm-dev';
        // endpoint per upload lato client su cloudinary
        utilities.cloudinary_upload_api = 'https://api.cloudinary.com/v1_1/plurimedia/upload';

        // i bottoni conferma e cancella di bootbox
        utilities.confirmButtons = {
            'cancel': {
                label: 'Annulla',
                className: 'btn-default'
            },
            'confirm': {
                label: 'Conferma',
                className: 'btn-primary'
            }
        };

        return utilities;
    })