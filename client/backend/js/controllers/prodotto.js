backend.controller('prodotto', [
    '$scope',
    '$location',
    '$timeout',
    '$window',
    '$modal',
    'Upload',
    'ngNotify',
    'UTILITIES',
    'MENUCATEGORIE',
    'PRODOTTO',
    function($scope,$location,$timeout,$window,$modal,Upload,ngNotify,UTILITIES,MENUCATEGORIE,PRODOTTO) {

    	$scope.lang = 'it';

        $scope.modalVariante = $modal({
            scope: $scope,
            templateUrl: '/backend/html/modals/nuova-variante.html',
            show: false,
            backdrop: 'static'
        });

        $scope.modalUploadImmagini = $modal({
            scope: $scope,
            templateUrl: '/backend/html/modals/caricamento-immagini.html',
            show: false,
            backdrop: 'static'
        });

        var url = $location.$$absUrl.split('/'),
            _id = url.length === 6 ? url.pop() : undefined;

        if(_id) {
            $scope.prodotto = PRODOTTO.get({id: _id}, function () {
                $scope.pageTitle = $scope.prodotto.codice +' - '+$scope.prodotto.it.nome;
            });
        }
        else {
            $scope.prodotto = { it: {} }
            $scope.pageTitle = 'Nuovo prodotto';
        } 
            

        var urlParams = new URLSearchParams(window.location.search);

        if(urlParams.has('saved'))
            ngNotify.set('Prodotto salvato', 'success');


        $scope.categorie = MENUCATEGORIE.get(function(cat){

                // sistemo per le option group: raggruppo per padri e lascio quelli senza figli fuori dai gruppi
                $scope.categorieGroups = [];

                function parseCategorie (menu,group) {
                    menu.voci.forEach(function(voce){
                        if(voce.voci.length){
                            $scope.categorieGroups.push({nome: voce.categoria.it.nome, group: voce.categoria.it.nome, _id: voce.categoria._id} )
                        }
                        else {
                            var cat = {nome: voce.categoria.it.nome, _id: voce.categoria._id} ;
                            if(group)
                                $scope.categorieGroups.push(_.extend(cat,{group: group}))
                            else
                                $scope.categorieGroups.push(cat)
                        }
                        
                        if(voce.voci.length){
                            parseCategorie(voce,voce.categoria.it.nome)
                        }
                    })
                }

                parseCategorie(cat)
        });

        $scope.filterOptions = function (option) {
            return option.nome!==option.group;
        }



        $scope.setIT = function () {
        	$scope.lang = 'it';
            // quando cambio se ha svuotato tutte le proprietà inglesi, cancello l'oggetto en e permetto di salvare
            if($scope.prodotto.en){
                if(_.compact(_.values($scope.prodotto.en)).length === 0)
                    delete $scope.prodotto.en;
            }
        }

        $scope.setEN = function () {
        	$scope.lang = 'en';   
        }


        // aggiunge una variante
        $scope.aggiungiVariante = function (variante) {

        	if($scope.prodotto[$scope.lang] && !$scope.prodotto[$scope.lang].varianti)
        		$scope.prodotto[$scope.lang].varianti = [];

            if(variante) {
                if($scope.prodotto[$scope.lang]) {
                    $scope.prodotto[$scope.lang].varianti.push({
                        tipo: variante,
                        opzioni: [{nome: '', prezzo: $scope.prodotto[$scope.lang] ? $scope.prodotto[$scope.lang].prezzo : 0}]
                    })

                    $scope.modalVariante.hide()
                    
                }
            }
        }

        // nuova opzione alla variante  
        $scope.nuovaOpzione = function (indexVariante) {

        	$scope.prodotto[$scope.lang].varianti[indexVariante].opzioni.push({
        		nome:'', prezzo: $scope.prodotto[$scope.lang] ? $scope.prodotto[$scope.lang].prezzo : 0
        	})
        }

        // elimina opzione della variante  
        $scope.eliminaOpzione = function (indexVariante,indexOpzione) {

        	$scope.prodotto[$scope.lang].varianti[indexVariante].opzioni.splice(indexOpzione,1);

        	if(!$scope.prodotto[$scope.lang].varianti[indexVariante].opzioni.length)
        		$scope.prodotto[$scope.lang].varianti.splice(indexVariante,1);

        }
        // trascinamento immagine principale: scatta l'upload su cloudinary
        $scope.uploadImmagine = function (files) {

            $scope.immagine = files[0]

            if($scope.immagine) {


                $scope.immagine.upload = Upload.upload({
                    url: UTILITIES.cloudinary_upload_api,
                    fields: {
                        upload_preset: UTILITIES.cloudinary_upload_preset
                    },
                    file: $scope.immagine,
                    skipAuthorization: true
                })
                $scope.immagine.upload.then(

                    function (response) {

                        console.log(response)
                        if (response.status === 200) {
                            $scope.prodotto.immagine = response.data.public_id
                            $scope.progress = false;
                        } else {
                            console.log('errore cricamento immagine')
                        }

                    },
                    function (resp) {
                        console.log('Error status: ' + resp.status);
                    },
                    function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.progress = progressPercentage; // aggiorno la percentuale
                    }
                );
            }
        }

        // trascinamento immagini gallery: scatta l'upload su cloudinary
        $scope.uploadImmagini = function (files) {

            $scope.immagini = files;
            var caricate = [];

            if ($scope.immagini.length) {

            	$scope.progressimmagini = true;

                $scope.modalUploadImmagini.show();

                async.each($scope.immagini, function (file, cb) {
                    file.upload = Upload.upload({
                        url: UTILITIES.cloudinary_upload_api,
                        fields: {
                            upload_preset: UTILITIES.cloudinary_upload_preset
                        },
                        file: file,
                        skipAuthorization: true
                    })
                    file.upload.then(

                        function (response) {
                            if (response.status === 200) {
                                caricate.push(response.data.public_id); // pusho
                                cb();
                            } else {
                                cb(response);
                            }

                        },
                        function (resp) {
                            console.log('Error status: ' + resp.status);
                        },
                        function (evt) {
                            var index = $scope.immagini.indexOf(file);
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            $scope.immagini[index].progress = progressPercentage; // aggiorno la percentuale del singolo file
                        }
                    );
                }, function (err) {
                    if (err)
                        ngNotify.set('Errore durante il caricamento delle immagini, riprova', 'error');

                    $scope.modalUploadImmagini.hide();

                    if(!$scope.prodotto.gallery)
                    	$scope.prodotto.gallery = [];

                    $scope.prodotto.gallery = _.union(caricate, $scope.prodotto.gallery); // aggiungo

                    $scope.progressimmagini = false;

                    ngNotify.set('Foto caricate', 'success');
                })
            }

        }

        $scope.cancellaImmagine = function (index) {
            $scope.prodotto.gallery.splice(index, 1)
        }

        $scope.disabled = function () {
            return $scope.formProdotto.$invalid ||
                   $scope.prodotto.en && $scope.prodotto.it.nome === $scope.prodotto.en.nome || 
                   $scope.prodotto.en && !$scope.prodotto.en.nome ||
                   $scope.prodotto.en && !$scope.prodotto.en.prezzo;
        }

        $scope.salvaProdotto = function (continua) {

            if (!$scope.prodotto._id) {

                PRODOTTO.save($scope.prodotto, function (prodotto) {
                        if(!continua){
                            $scope.prodotto = prodotto;
                            $scope.pageTitle = $scope.prodotto.codice +' - '+$scope.prodotto.it.nome;
                            ngNotify.set('Prodotto salvato', 'success');
                        }
                        else {
                            $window.location.href = '/admin/prodotto?saved=ok';
                        }
                    },
                    function (err) {
                        console.log(err)
                        ngNotify.set('Errore durante il salvataggio del prodotto', 'error');
                    });
            } 

            else {

                PRODOTTO.update($scope.prodotto,function (prodotto) {
                    $scope.prodotto = prodotto;
                     $scope.pageTitle = $scope.prodotto.codice +' - '+$scope.prodotto.it.nome;
                    ngNotify.set('Prodotto aggiornato', 'success');
                },function (err) {
                    console.log(err)
                    ngNotify.set('Errore durante l’aggiornamento del prodotto', 'error');
                });

            }    	
        }

        $scope.eliminaProdotto = function () {
            bootbox.confirm({

                title: 'Elimina prodotto',
                message: 'Sei sicuro di voler eliminare questo prodotto? Non sarà più visibile sul sito',
                buttons: UTILITIES.confirmButtons,
                callback: function (ok) {
                    if (ok) {
                        $scope.prodotto.$delete(function () {
                            $window.location.href = '/admin/prodotti?delete=ok';

                        },function (err) {
                            console.log(err)
                            ngNotify.set('Errore durante l’eliminazione del prodotto', 'error');
                        });
                    }
                }
            });
        }

        $scope.duplicaProdotto = function () {

            $scope.copia = _.omit(_.cloneDeep($scope.prodotto),['_id'])
            $scope.copia.codice = $scope.copia.codice + ' - COPIA'
            $scope.copia.it.nome = $scope.copia.it.nome + ' - COPIA'
            delete $scope.copia.it.url;
            if($scope.copia.it.redirect)
               delete $scope.copia.it.redirect 
            if($scope.copia.en && $scope.copia.en.url)
                delete $scope.copia.en.url;
            if($scope.copia.en && $scope.copia.en.redirect)
               delete $scope.copia.en.redirect

            PRODOTTO.save($scope.copia, function (prodotto) {

                         ngNotify.set('Prodotto duplicato', 'success');

                         $timeout(function(){
                            $window.location.href = '/admin/prodotto/'+prodotto._id;    
                         },2000)

                    },
                    function (err) {
                        console.log(err)
                        ngNotify.set('Errore durante la duplicazione del prodotto', 'error');
                    });
        }

    }
]);
