backend.controller('ordini', [
    '$scope',
    'ngNotify',
    'ORDINE',
    function($scope,ngNotify,ORDINE) {

        $scope.ordini = ORDINE.query(({limit:0}));
        $scope.cerca = {};


        $scope.reset = function () {
            $scope.ordini = ORDINE.query({limit:0 });
            $scope.searchresults = false;   	
        }

        $scope.cercaOrdini = function () {
            $scope.ordini = ORDINE.query($scope.cerca, function (r) {
                console.log(r)
                $scope.searchresults = true;
            })        	
        }
    }
]);
