backend.controller('categoriaOrdina', [
    '$scope',
    '$timeout',
    '$location',
    'ngNotify',
    'CATEGORIA',
    'PRODOTTO',
    'CATEGORIEVIDENZA',
    function($scope,$timeout,$location,ngNotify,CATEGORIA,PRODOTTO,CATEGORIEVIDENZA) {

        var url = $location.$$absUrl.split('/');
        
        $scope._id = url[5]
        $scope.categoria = CATEGORIA.get({id: $scope._id});
        $scope.prodotti = PRODOTTO.query({categoria: $scope._id,sort:'peso,1'})

        $scope.salvaOrdine = function () {

        	$scope.saving = true;

        	async.each($scope.prodotti, function (prodotto, cb){

        		prodotto.peso = $scope.prodotti.indexOf(prodotto);

                PRODOTTO.update(prodotto,function () {
                	cb()
                },function (err) {
                	cb(err)
                });        		

        	},function(err){

        		if(err)
        			ngNotify.set('Ops, qualcosa è andato storto, riprova', 'error');

                // aggiorno le catgorie in evidenza con il nuovo ordine 
                

                CATEGORIEVIDENZA.get(function(result){
                    CATEGORIEVIDENZA.update(result,function (evidenza) {
                        $timeout(function(){
                            $scope.saving = false;
                            ngNotify.set('Ordinamento salvato', 'success');                 
                        },1000)

                    },function (err) {
                        ngNotify.set('Ops, qualcosa è andato storto, riprova', 'error');
                    });                    
                })

        	})

        }

    }
]);
