backend.controller('ordine', [
    '$scope',
    '$location',
    'ngNotify',
    'EMAIL',
    'ORDINE',
    function($scope,$location,ngNotify,EMAIL,ORDINE) {

        var url = $location.$$absUrl.split('/'),
            _id = url.length === 6 ? url.pop() : undefined;

        $scope.ordine = ORDINE.get({id: _id}, function (ordine) {

            $scope.pageTitle = ordine.numero;

            if($scope.ordine.paypal)
                $scope.paypal_fee = parseFloat($scope.ordine.paypal.payment_fee)

            EMAIL.status({id: ordine.mail_cliente[0]._id}, function(stato){
                $scope.emailCliente = stato;
            })
        });


        $scope.cambiaStato = function (stato) {
            $scope.ordine.stato = stato;

            ORDINE.update($scope.ordine,function (ordine) {
                $scope.ordine = ordine;
                ngNotify.set('Ordine aggiornato', 'success');
            },function (err) {
                console.log(err)
                ngNotify.set('Errore durante l’aggiornamento dell\'ordine', 'error');
            });
        }
    }
]);
