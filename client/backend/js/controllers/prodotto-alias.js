backend.controller('prodottoAlias', [
    '$scope',
    '$location',
    'ALIAS',
    'PRODOTTO',
    function($scope,$location,ALIAS,PRODOTTO) {

        var url = $location.$$absUrl.split('/');
        
        $scope._id = url[5]

        $scope.prodotto = PRODOTTO.get({id: $scope._id}, function(){
        	$scope.pageTitle = $scope.prodotto.codice +' - '+$scope.prodotto.it.nome;
        });

        $scope.alias = ALIAS.query({document: $scope._id })

    }
]);
