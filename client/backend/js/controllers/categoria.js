backend.controller('categoria', [
    '$scope',
    '$location',
    '$window',
    'Upload',
    'ngNotify',
    'UTILITIES',
    'ALIAS',
    'CATEGORIA',
    function($scope,$location,$window,Upload,ngNotify,UTILITIES,ALIAS,CATEGORIA) {

		$scope.lang = 'it';
        var url = $location.$$absUrl.split('/'),
            _id = url.length === 6 ? url.pop() : undefined;

        // sono nel dettaglio di una esistente
        if(_id) {
            $scope.categoria = CATEGORIA.get({id: _id}, function (){
                $scope.pageTitle = $scope.categoria.it.nome;
            });
        }
        else {
            $scope.categoria = {}; // nuova
            $scope.pageTitle = 'Nuova categoria';
        }


        var urlParams = new URLSearchParams(window.location.search);

        if(urlParams.has('saved'))
            ngNotify.set('Categoria salvata', 'success');

        $scope.setIT = function () {
            $scope.lang = 'it';
            // quando cambio se ha svuotato tutte le proprietà inglesi, cancello l'oggetto en e permetto di salvare
            if($scope.categoria.en){
                if(_.compact(_.values($scope.categoria.en)).length === 0)
                    delete $scope.categoria.en;
            }
        }

        $scope.setEN = function () {
            $scope.lang = 'en';
        }

        // trascinamento dei file: scatta l'upload su cloudinary
        $scope.uploadImmagine = function (files) {

            $scope.immagine = files[0]

            if($scope.immagine) {

                $scope.immagine.upload = Upload.upload({
                    url: UTILITIES.cloudinary_upload_api,
                    fields: {
                        upload_preset: UTILITIES.cloudinary_upload_preset
                    },
                    file: $scope.immagine,
                    skipAuthorization: true
                })
                $scope.immagine.upload.then(

                    function (response) {
                        if (response.status === 200) {
                            $scope.categoria.immagine = response.data.public_id

                            $scope.progress = false;
                        } else {
                            console.log('errore cricamento immagine')
                        }

                    },
                    function (resp) {
                        console.log('Error status: ' + resp.status);
                    },
                    function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.progress = progressPercentage; // aggiorno la percentuale
                    }
                );
            }
        }

        $scope.disabled = function () {
            return $scope.formCategoria.$invalid ||
                   $scope.categoria.en && $scope.categoria.it.nome === $scope.categoria.en.nome || 
                   $scope.categoria.en && !$scope.categoria.en.nome;

        }

        // salvataggio categoria
        $scope.salvaCategoria = function (continua) {


            if (!$scope.categoria._id) {

                CATEGORIA.save($scope.categoria, function (categoria) {
                        if(!continua){
                            $scope.categoria = categoria;
                            $scope.pageTitle = categoria.it.nome;                            
                            ngNotify.set('Categoria salvata', 'success');
                        }
                        else{
                            $window.location.href = '/admin/categoria?saved=ok';
                        }

                    },
                    function (err) {
                        console.log(err)
                        ngNotify.set('Errore durante il saltaggio della categoria', 'error');
                    });
            } 

            else {

                CATEGORIA.update($scope.categoria,function (categoria) {
                    $scope.categoria = categoria;
                    $scope.pageTitle = categoria.it.nome;
                    ngNotify.set('Categoria aggiornata', 'success');
                },function (err) {
                    console.log(err)
                    ngNotify.set('Errore durante l’aggiornamento della categoria', 'error');
                });
            }
        }

    }
]);
