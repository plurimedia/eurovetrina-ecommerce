backend.controller('categoriaAlias', [
    '$scope',
    '$location',
    'ALIAS',
    'CATEGORIA',
    function($scope,$location,ALIAS,CATEGORIA) {

        var url = $location.$$absUrl.split('/');
        
        $scope._id = url[5]

        $scope.categoria = CATEGORIA.get({id: $scope._id});

        $scope.alias = ALIAS.query({document: $scope._id })

    }
]);
