backend.controller('categorie', [
    '$scope',
    'ngNotify',
    'UTILITIES',
    'MENUCATEGORIE',
    function($scope,ngNotify,UTILITIES,MENUCATEGORIE) {

    	/* recupero le categorie e il menu  */

    	$scope.menuCategorie = MENUCATEGORIE.get();

        $scope.treeOptions = {
          defaultCollapsed: false,
          dropped: function(event) {
            $scope.bozza = true;
          }    
        }

        $scope.selezionaCategoria= function (categoria) {

          $scope.voceEsiste = false;

          function cercaVoce (voci) {

            voci.forEach(function(voce){

              if(voce.categoria._id === categoria._id){
                $scope.voceEsiste = true;
              }
              else {
                if(voce.voci && voce.voci.length)
                  cercaVoce(voce.voci)
              }
            })
          } 

          if($scope.menuCategorie.voci)
          	cercaVoce($scope.menuCategorie.voci)

          else
          	$scope.menuCategorie.voci = [];

          if(!$scope.voceEsiste) {

            $scope.menuCategorie.voci.push({
              categoria: categoria,
            	voci: []
            })
            ngNotify.set('Categoria aggiunta', 'success');
            $scope.bozza = true;

          }
          else {
            ngNotify.set('hai già inserito questa categoria nel menu', 'warn');
          }

        }

        $scope.cambiaVisibilita = function () {
          $scope.bozza = true;
        }

        // salvataggio menu
        $scope.salvaMenu = function () {

            if (!$scope.menuCategorie._id) {

                MENUCATEGORIE.save($scope.menuCategorie, function (menucategoria) {
                        $scope.menuCategorie = menucategoria;
                        ngNotify.set('Menu salvato', 'success');
                    },
                    function (err) {
                        console.log(err)
                        ngNotify.set('Errore durante il salvataggio del menu', 'error');
                    });
            } 

            else {

                MENUCATEGORIE.update($scope.menuCategorie,function (menucategoria) {
                    $scope.menuCategorie = menucategoria;
                    ngNotify.set('Menu aggiornato', 'success');
                },function (err) {
                    console.log(err)
                    ngNotify.set('Errore durante l’aggiornamento del menu', 'error');
                });
            }

            $scope.bozza = false;
        }

    }
]);
