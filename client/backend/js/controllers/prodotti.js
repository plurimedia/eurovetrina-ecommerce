backend.controller('prodotti', [
    '$scope',
    'ngNotify',
    'MENUCATEGORIE',
    'PRODOTTO',
    function($scope,ngNotify,MENUCATEGORIE,PRODOTTO) {

        $scope.prodotti = PRODOTTO.query(({limit:20,sort:'mdate,-1'}));
        $scope.cerca = {};

        var urlParams = new URLSearchParams(window.location.search);

        if(urlParams.has('delete'))
            ngNotify.set('Prodotto eliminato', 'success');


        $scope.categorie = MENUCATEGORIE.get(function(cat){

                // sistemo per le option group: raggruppo per padri e lascio quelli senza figli fuori dai gruppi
                $scope.categorieGroups = [];

                function parseCategorie (menu,group) {
                    menu.voci.forEach(function(voce){
                        if(voce.voci.length){
                            $scope.categorieGroups.push({nome: voce.categoria.it.nome, group: voce.categoria.it.nome, _id: voce.categoria._id} )
                        }
                        else {
                            var cat = {nome: voce.categoria.it.nome, _id: voce.categoria._id} ;
                            if(group)
                                $scope.categorieGroups.push(_.extend(cat,{group: group}))
                            else
                                $scope.categorieGroups.push(cat)
                        }
                        
                        if(voce.voci.length){
                            parseCategorie(voce,voce.categoria.it.nome)
                        }
                    })
                }

                parseCategorie(cat)
        });        


        $scope.reset = function () {
            $scope.prodotti = PRODOTTO.query({limit:20,sort:'mdate,-1'});
            $scope.searchresults = false;   	
            $scope.cerca = {};
        }

        $scope.cercaProdotti = function () {
            $scope.prodotti = PRODOTTO.query($scope.cerca, function () {
                $scope.searchresults = true;
            })        	
        }
    }
]);
