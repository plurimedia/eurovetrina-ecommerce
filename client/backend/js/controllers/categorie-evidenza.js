backend.controller('categorieEvidenza', [
    '$scope',
    'Upload',
    'ngNotify',
    'CATEGORIA',
    'CATEGORIEVIDENZA',
    'UTILITIES',
    function($scope,Upload,ngNotify,CATEGORIA,CATEGORIEVIDENZA,UTILITIES) {

    	
    	$scope.categorie = CATEGORIA.query({});
    	$scope.evidenza = CATEGORIEVIDENZA.get(function(result){
    		if(!result._id) {
                $scope.blocco={it:'',en:''}
    			$scope.evidenza = {
    				categorie : []
    			};
    		}
    		else {
    			$scope.evidenza.categorie = result.categorie;
    		}
    	});



    	$scope.aggiungiBlocco = function () {
    		$scope.evidenza.categorie.push(_.extend($scope.blocco,{figlie:[]}))
            $scope.blocco = {it:'',en:''};
    	}

    	$scope.aggiungiFiglia = function (index,figlia) {
    		$scope.evidenza.categorie[index].figlie.push(figlia)
    	}

    	$scope.disponibili = function (cat) {

    		if($scope.evidenza.categorie && $scope.evidenza.categorie.length > 0) {
	    		$scope.usate = [];
	    		$scope.evidenza.categorie.forEach(function(c){
	    			$scope.usate.push(c._id);
	    			if(c.figlie){
	    				c.figlie.forEach(function(f){
	    					$scope.usate.push(f._id)
	    				})
	    			}
	    		})

	    		return $scope.usate.indexOf(cat._id)===-1   			
    		}

    		else {
    			return true;
    		}

    	}

        // trascinamento dei file: scatta l'upload su cloudinary
        $scope.uploadImmagine = function (files, index) {

        	$scope.progress = {}

        
            $scope.immagine = files[0]

            if($scope.immagine) {

                $scope.immagine.upload = Upload.upload({
                    url: UTILITIES.cloudinary_upload_api,
                    fields: {
                        upload_preset: UTILITIES.cloudinary_upload_preset
                    },
                    file: $scope.immagine,
                    skipAuthorization: true
                })
                $scope.immagine.upload.then(

                    function (response) {

             
                        if (response.status === 200) {
                        	_.extend($scope.evidenza.categorie[index],{immagine : response.data.public_id})
                            

                            $scope.progress[index] = false;
                        } else {
                            console.log('errore cricamento immagine')
                        }

                    },
                    function (resp) {
                        console.log('Error status: ' + resp.status);
                    },
                    function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.progress[index] = progressPercentage; // aggiorno la percentuale
                    }
                );
            }
        }

        $scope.salva = function () {
            if (!$scope.evidenza._id) {

                CATEGORIEVIDENZA.save($scope.evidenza, function (evidenza) {
                        $scope.evidenza.categorie = evidenza.categorie;
                        ngNotify.set('Categorie in evidenza salvate', 'success');
                    },
                    function (err) {
                        console.log(err)
                        ngNotify.set('Errore durante il salvataggio delle categorie in evidenza', 'error');
                    });
            } 

            else {

                CATEGORIEVIDENZA.update($scope.evidenza,function (evidenza) {
                    $scope.evidenza.categorie = evidenza.categorie;
                    ngNotify.set('Categorie in evidenza aggiornate', 'success');
                },function (err) {
                    ngNotify.set('Errore durante l\'aggiornamento delle categorie in evidenza', 'error');
                });
            }

        }

    }
]);
