numeral.locale('it');

var frontend = angular.module('frontend', [
  'ngResource', // services
  'ngCookies',
  'ngNotify'
])
.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('[[').endSymbol(']]');
})
.run(function($rootScope, CART, USER) {

    $rootScope.cart = CART.get();
    $rootScope.user = USER.get();

})
.service('PROVINCE', function($http) {
    return {
        query: function() {
            return $http.get('./frontend/json/province.json', {}).then(function(resp) {
                return _.sortBy(resp.data,'nome');
            });
        }
    };
})
.service('USER', ['$resource',
    function($resource) {
        return $resource('/api/user/:id', {
            id: '@user_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
])
.service('USERS', ['$resource',
    function($resource) {
        return $resource('/api/users');
    }
])
.service('CART', ['$resource',
    function($resource) {
        return $resource('/api/cart/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
])
.service('ORDER', ['$resource',
    function($resource) {
        return $resource('/api/order/:id', {
            id: '@user_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
])
.service('CONTACT', ['$resource',
    function($resource) {
        return $resource('/api/contact');
    }
])
.directive('cloudinary', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            id: '@',
            transformation: '@'
        },
        template: '<img ng-src="https://res.cloudinary.com/plurimedia/image/upload/{{transformation}}/{{id}}"/>'
    }
})
.filter('euro', function () {
    return function (input) {
        if(input === 0)
            return numeral(0).format('$0,0.00')
        else
            return input ? numeral(input).format('$0,0.00') : '';
    }
})
.factory('UTILITIES', function () {
    var utilities = {};

    /* setta un messaggio di errore con indicazioni per richiedere assistenza */
    utilities.roundToTwo = function (number) {
       return(Math.round(number * 100) / 100);
    }

    return utilities;

})
.controller('prodotto', [
    '$scope',
    '$rootScope',
    '$cookies',
    'ngNotify',
    '$filter',
    '$window',
    'CART',
    function($scope,$rootScope,$cookies,ngNotify,$filter,$window, CART) {

        $scope.prodotto = prodotto;


        $scope.init = function () {
            $scope.scelta = { codice: prodotto.codice, immagine:prodotto.immagine, prezzo: prodotto.prezzo, url: prodotto.url, nome: prodotto.nome,qta: 0 };
        }

        $scope.init();


        if($scope.prodotto.varianti) {
            $scope.prodotto.varianti.forEach(function(v){
                v.opzioni.forEach(function(o){
                    o.format = o.nome +' (' +$filter('euro')(o.prezzo) + ')'
                })
            })
        }

        $scope.addToCartDisabled = function(){
        	if($scope.scelta.qta < 1)
        		return true;
        	if($scope.prodotto.varianti && $scope.prodotto.varianti.length && !$scope.scelta.opzione)
        		return true;
        }

        $scope.addToCart = function () {

            CART.save($scope.scelta, function (cart){
                $rootScope.cart = cart;
                ngNotify.set('Prodotto aggiunto al carrello', 'success');
                $scope.scelta.qta = 0;
            },
            function(){
                ngNotify.set('Si è verificato un errore, riprova','error')
            })
        }

        $scope.selezioneVariante = function () {
            $scope.scelta.prezzo = $scope.scelta.variante.prezzo
            $scope.scelta.opzione = $scope.scelta.variante.format
        }
    }
])
.controller('carrello', [
    '$scope',
    '$http',
    '$rootScope',
    '$timeout',
    '$window',
    'ngNotify',
    'PROVINCE',
    'USER',
    'USERS',
    'CART',
    'ORDER',
    'UTILITIES',
    function($scope,$http,$rootScope,$timeout,$window,ngNotify,PROVINCE, USER, USERS, CART, ORDER,UTILITIES) {


        PROVINCE.query().then(function (p) {
            $scope.province = p;
        });

        if (!$scope.user) $scope.user = {};

        $scope.zips = [80073,80077,57031,57033,57033,98055,98050,92010,58012,91017,04027,71040,23030,80067,84011,84017,07024,19016,19018,19017,19025];
        // aggiungo quelli di venezia
        _.times(55, function(i) {
            $scope.zips.push(30121 + i)
        })

        /*$scope.shipCalc = function () {
            /*if($rootScope.cart.zip){
                if($scope.zips.indexOf(parseInt($rootScope.cart.zip))!==-1)
                    $rootScope.cart.speseSpedizione = 30;
                else
                    $rootScope.cart.speseSpedizione = 15;
            }* /

            $rootScope.cart.speseSpedizione = 12;
        }*/

        $scope.updateCart = function (msg, callback) {

            CART.update($rootScope.cart, function(cart) {

                if(msg)
                    ngNotify.set(msg, 'success');

                $rootScope.cart = cart;

                if(callback)
                    callback();
            },
            function(err){
                ngNotify.set('Aggiornamento del carrello non riuscito', 'error');
            })

        }

        $scope.cambiaQta = function (n) {
            if (n > 0) {
                $timeout(function() {
                    $scope.updateCart('Quantità aggiornata');
                    // $scope.shipCalc()
                }, 1000)
            }
        }

    	$scope.cartRemove = function (index) {
            $rootScope.cart.prodotti.splice(index,1)
            // $scope.shipCalc()
            $scope.updateCart('Prodotto eliminato dal carrello');
        }

        $scope.emptyCart = function () {
            $rootScope.cart = {prodotti: [], count:0};
            // $scope.shipCalc()
            $scope.updateCart('Carrello svuotato');
        }

        $scope.subTotale = function () {
            var tot = _.sumBy($rootScope.cart.prodotti, function(c) {
                return c.qta * c.prezzo;
            })

            if (tot<100)
                $rootScope.cart.speseSpedizione = 12;
            else
                $rootScope.cart.speseSpedizione = 12;

            return UTILITIES.roundToTwo(tot);
        }

        /* sconto ulteriore del 20 */
        $scope.scontoNatalizio = function () {
            // return UTILITIES.roundToTwo($scope.subTotale() / 100 * 20);
            return 0;
        }

        $scope.sconto10 = function () {
            // return $scope.subTotale() >=1500 ? ($scope.subTotale() - $scope.scontoNatalizio()) / 100 * 10 : 0;
            return $scope.subTotale() >=1500 ? $scope.subTotale() / 100 * 10 : 0;
        }

        $scope.sconto = function () {
            // return  $scope.scontoNatalizio() + $scope.sconto10();
            return  $scope.sconto10();
        }

        $scope.totaleIva = function () {
            return  UTILITIES.roundToTwo($scope.imponibile() / 100 * 22)
        }

        $scope.imponibile = function () {
            // return  UTILITIES.roundToTwo($scope.subTotale() - $scope.sconto10() - $scope.scontoNatalizio() + $rootScope.cart.speseSpedizione); /* togliere sconto extra alla fine della promo*/
            // return UTILITIES.roundToTwo($scope.subTotale() - $scope.sconto10() + $rootScope.cart.speseSpedizione);
            return UTILITIES.roundToTwo($scope.subTotale() - $scope.sconto10() + $rootScope.cart.speseSpedizione);
        }

        $scope.totale = function () {
            return  UTILITIES.roundToTwo($scope.imponibile() + $scope.totaleIva());
        }

        $scope.validOrder = function () {
           
            if ($scope.imponibile() < 0) return false; 
            // ordine minimo 0 € cambiato in data 17/01/25
            // se oridne minimo è da mettere a 100 € mettete 110 invece che 0 così: (if ($scope.imponibile() < 110))

           /* if($scope.subTotale()>0) {
                if($scope.subTotale()>=500 && $rootScope.cart.speseSpedizione === 0)
                    return true;
                if($scope.subTotale()<500 && $rootScope.cart.speseSpedizione > 0)
                    return true;
                return true
            }
            else
                return false;*/
            return true;
            // return $scope.imponibile() >= 100
        }

        $scope.checkout = function () {
            console.log('checkout')
            $scope.saving = true;
            //aggiorno il carrello
            _.extend($rootScope.cart,{
                subTotale: $scope.subTotale(),
                // scontoNatalizio: $scope.scontoNatalizio(), // da togliere
                sconto10: $scope.sconto10(),
                sconto: $scope.sconto(),
                imponibile: $scope.imponibile(),
                totaleIva: $scope.totaleIva(),
                totale: $scope.totale()
            })
            $scope.updateCart(null, function() {
                 $timeout(function() {
                    $window.location.href = 'carrello-consegna';
                }, 1000)
            });
        }

        $scope.canPay = function () {
            // questa chiamata è SOLO DI TEST per controllare il funzionamento del client di auth0. Commentare a fine sviluppo e non cancellarla
            /* if (!$scope.utentiTest) {
                $scope.utentiTest  = 1
                USERS.get()
            } */
            
            try {
                var ok = !$rootScope.cart.fattura && $scope.formSpedizione.$valid ||
                         $rootScope.cart.fattura && $scope.formSpedizione.$valid && $scope.formFatturazione.$valid;

                if($rootScope.user.user_id){
                    return ok;
                }
                else {
                    return ok &&
                           $scope.formAccount.$valid &&
                           $scope.user.email == $scope.confermaEmail &&
                           $scope.user.password == $scope.confermaPassword;
                }
            }
            catch(err){
                return false;
            }
        }

        $scope.vaiPagamento = function () {
            // aggiorno le preferenze utente
            $scope.user.user_metadata.fattura = $rootScope.cart.fattura;

            $scope.updateCart(null,function(){
                if (!$scope.user.user_id) { // anonimo creo utente prima

                    $scope.savingUser = true;

                    USER.save($scope.user, function (response) {
                        $scope.toVerify = true;
                    },
                    function (err) {
                        var msg = 'Errore durante la creazione dell’utente, riprova';

                        if (err.data.indexOf('exists')!==-1)
                            msg = 'L’utente esiste già'
                        $scope.savingUser = false;
                        ngNotify.set(msg,{type:'error',sticky: true});
                    })
                } else {
                    $scope.savingUser = true;
                    // aggiorno
                    USER.update($scope.user, function (response){
                        $scope.savingUser = false;
                       $rootScope.user = response;
                       // $window.location.href = 'carrello-pagamento'
                        $timeout(function() {
                            $window.location.href = 'carrello-pagamento';
                        }, 1000)
                    },
                    function (err){
                        console.log(err)
                        var msg = 'Errore durante l’aggiornamento dell’utente, riprova';
                        $scope.savingUser = false;
                        ngNotify.set(msg,'error');
                    })

                }
            });
        }

        $scope.vaiRiepilogo = function (tipoPagamento) {
            $rootScope.cart.pagamento = tipoPagamento; // segno

            $scope.updateCart(null, function() {
                $timeout(function() {
                    $window.location.href = 'carrello-riepilogo';
                }, 100)
            });
        }

        $scope.completaOrdine = function () {
            $scope.saving = true;
            ORDER.save({}, function(ordine) {
                // finita qui
                if (ordine.carrello.pagamento !== 'carta') {
                    $timeout(function() {
                        // faccio una post come paypal
                        document.getElementById('pagamentoform').submit()
                    }, 100)
                } else { // mando a paypal
                    //valorizzo prima il numero ordine
                    $scope.saving = false;
                    $scope.waitingpaypal = true;
                    document.getElementById('paypalform-ordine').value = ordine.numero;
                    document.getElementById('paypalform').submit()
                    return false;
                }
            }, function(err) {
                ngNotify.set('Si è verificato un erore, riprova','error')
            });
        }

        $scope.continuaShopping = function () {
            $scope.updateCart(null, function() {
                $window.location.href = '/categorie';
            });
        }
    }
])
.controller('user', [
    '$scope',
    '$rootScope',
    '$cookies',
    'ngNotify',
    '$filter',
    '$window',
    'USER',
    function($scope,$rootScope,$cookies,ngNotify,$filter,$window,USER) {

        $scope.user = user;

        $scope.canUpdate = function(){
            if(!$scope.user.user_metadata.fattura)
                return $scope.formSpedizione.$valid;
            else
                return $scope.formSpedizione.$valid && $scope.formFatturazione.$valid;
        }

        $scope.aggiornaUtente = function () {

            USER.update($scope.user, function (response){
               $rootScope.user = response;
               ngNotify.set('Dati aggiornati','success')
            },
            function (err){
                var msg = 'Errore durante l’aggiornamento dell’utente, riprova';
                $scope.savingUser = false;
                ngNotify.set(msg,'error');
            })
        }
    }
])
.controller('contatti', [
    '$scope',
    '$rootScope',
    '$window',
    'CONTACT',
    function($scope,$rootScope,$window,CONTACT) {

        $rootScope.user.$promise.then(function(user){
            if(user.email){
                $scope.contact = {
                    nome: user.user_metadata.nome,
                    cognome: user.user_metadata.cognome,
                    email: user.email,
                    telefono: user.user_metadata.telefono,
                };
            }
            else {
                $scope.contact = {}
            }
        })

        $scope.inviato = false;

        $scope.invia = function () {
            CONTACT.save($scope.contact, function(r){
                $scope.inviato = true;
            },function(err){
                $scope.inviato = false;
            })
        }
    }
]);
