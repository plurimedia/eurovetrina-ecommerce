FROM node:10-alpine

# Define Variables
ARG ApplicationName="eurovetrina"
ARG ApplicationRoot="/opt/app-root/$ApplicationName"

# Install dependencies and fonts
RUN apk update && apk add --no-cache git ttf-liberation ttf-opensans

# Copy directories
COPY ./client "$ApplicationRoot"/client
COPY ./server "$ApplicationRoot"/server
COPY ./scripts "$ApplicationRoot"/scripts
COPY ./i18n "$ApplicationRoot"/i18n

# Copy files
COPY ./.nvmrc "$ApplicationRoot"/.nvmrc
COPY ./Procfile "$ApplicationRoot"/Procfile
COPY ./index.js "$ApplicationRoot"/index.js
COPY ./config.js "$ApplicationRoot"/config.js
COPY ./package.json "$ApplicationRoot"/package.json
COPY ./ibm_mongodb_ca.pem "$ApplicationRoot"/ibm_mongodb_ca.pem

WORKDIR "$ApplicationRoot"

RUN npm install && \
  npm install --only=dev

# Setting permissions for the non-root user
RUN chown -R node:node /opt && \
    chmod g+rwX -R /opt
    
USER node

CMD node app.js