require('dotenv-flow').config()

/*
 * Configurazione in base agli ambienti
 * E' necessario settare la variabile "ambiente" 
 */
var config = {
    production: {
        TITLE: 'Eurovetrina',
        URL: 'https://www.eurovetrinaespositori.it/',
        MONGO: process.env.MONGOURL, // 'mongodb://eurovetrina:V=y7tyH8;J8^XGU@ds153610-a0.mlab.com:53610,ds153610-a1.mlab.com:53610/heroku_gw49016w?replicaSet=rs-ds153610',
        AUTH0_TOKEN: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5UZzBSalF6UlRVMk1qRTNSRE5CTWtFMU1UZEZORFk0TmtGRE1qVkVOMFExUXpReE16YzJPQSJ9.eyJpc3MiOiJodHRwczovL3BsdXJpbWVkaWEuZXUuYXV0aDAuY29tLyIsInN1YiI6IkVSWldkYzJ0UUt0MFlkVzBldWk3MXZNcUNMbElsaW5uQGNsaWVudHMiLCJhdWQiOiJodHRwczovL3BsdXJpbWVkaWEuZXUuYXV0aDAuY29tL2FwaS92Mi8iLCJpYXQiOjE2NDQ5MzM5MzIsImV4cCI6MTY1MTMzMzkzMiwiYXpwIjoiRVJaV2RjMnRRS3QwWWRXMGV1aTcxdk1xQ0xsSWxpbm4iLCJzY29wZSI6InJlYWQ6Y2xpZW50X2dyYW50cyBjcmVhdGU6Y2xpZW50X2dyYW50cyBkZWxldGU6Y2xpZW50X2dyYW50cyB1cGRhdGU6Y2xpZW50X2dyYW50cyByZWFkOnVzZXJzIHVwZGF0ZTp1c2VycyBkZWxldGU6dXNlcnMgY3JlYXRlOnVzZXJzIHJlYWQ6dXNlcnNfYXBwX21ldGFkYXRhIHVwZGF0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgZGVsZXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBjcmVhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGNyZWF0ZTp1c2VyX3RpY2tldHMgcmVhZDpjbGllbnRzIHVwZGF0ZTpjbGllbnRzIGRlbGV0ZTpjbGllbnRzIGNyZWF0ZTpjbGllbnRzIHJlYWQ6Y2xpZW50X2tleXMgdXBkYXRlOmNsaWVudF9rZXlzIGRlbGV0ZTpjbGllbnRfa2V5cyBjcmVhdGU6Y2xpZW50X2tleXMgcmVhZDpjb25uZWN0aW9ucyB1cGRhdGU6Y29ubmVjdGlvbnMgZGVsZXRlOmNvbm5lY3Rpb25zIGNyZWF0ZTpjb25uZWN0aW9ucyByZWFkOnJlc291cmNlX3NlcnZlcnMgdXBkYXRlOnJlc291cmNlX3NlcnZlcnMgZGVsZXRlOnJlc291cmNlX3NlcnZlcnMgY3JlYXRlOnJlc291cmNlX3NlcnZlcnMgcmVhZDpkZXZpY2VfY3JlZGVudGlhbHMgdXBkYXRlOmRldmljZV9jcmVkZW50aWFscyBkZWxldGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGNyZWF0ZTpkZXZpY2VfY3JlZGVudGlhbHMgcmVhZDpydWxlcyB1cGRhdGU6cnVsZXMgZGVsZXRlOnJ1bGVzIGNyZWF0ZTpydWxlcyByZWFkOmVtYWlsX3Byb3ZpZGVyIHVwZGF0ZTplbWFpbF9wcm92aWRlciBkZWxldGU6ZW1haWxfcHJvdmlkZXIgY3JlYXRlOmVtYWlsX3Byb3ZpZGVyIGJsYWNrbGlzdDp0b2tlbnMgcmVhZDpzdGF0cyByZWFkOnRlbmFudF9zZXR0aW5ncyB1cGRhdGU6dGVuYW50X3NldHRpbmdzIHJlYWQ6bG9ncyByZWFkOnNoaWVsZHMgY3JlYXRlOnNoaWVsZHMgZGVsZXRlOnNoaWVsZHMgdXBkYXRlOnRyaWdnZXJzIHJlYWQ6dHJpZ2dlcnMgcmVhZDpncmFudHMgZGVsZXRlOmdyYW50cyByZWFkOmd1YXJkaWFuX2ZhY3RvcnMgdXBkYXRlOmd1YXJkaWFuX2ZhY3RvcnMgcmVhZDpndWFyZGlhbl9lbnJvbGxtZW50cyBkZWxldGU6Z3VhcmRpYW5fZW5yb2xsbWVudHMgY3JlYXRlOmd1YXJkaWFuX2Vucm9sbG1lbnRfdGlja2V0cyByZWFkOnVzZXJfaWRwX3Rva2VucyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.DSiStmCJ89e4RQbOC5Q9MGuoUeg22Rh9KCUH7N2Fqc8g0mgynSOICIgtqLExTqqv8ZZjtdN9U83vE5NfBpr-RYzTY9rcOHtoaEq7mNgACTLf-M02pyjPGFJ3_Ut74dcWJkuE2EjgRRLncMiso8nnjfwQYqXN6TYlyrJOZiiuGqWykUc9Tes0fZOpJHOkLLOxaOlvUsQg4omuFQ7vY8P6lafYUup-gTE7dNyAIsbSIazOo5u325UKbzxD6HTNDX_nCo4J8KJAAsh5yMm6dc7DljAzDtEyR6l-LYe1cXi8u8ORjM9F8B5qnxk8HWt6YwSoDia-jfaAqgwkO-PA9YhJpg',
        AUTH0_CLIENT_SECRET: 'PJnQ9vHHe0zHsubYU1EbbOzw6NFt1ABl3yQmxbGi8JFuK6SXJ49aQP3KEOyCCdG3',
        AUTH0_CLIENT_ID: 'u04viEQz2xqLW0Q81EE4n4Jvlxf1WKoc',
        AUTH0_CONNECTION: 'eurovetrina-ecommerce-prod',
        AUTH0_CALLBACK_URL: 'https://www.eurovetrinaespositori.it/auth0callback',
        AUTH0_DOMAIN: /* 'auth0.plurimedia.it', */ 'plurimedia.eu.auth0.com',
        MANDRILL_API_KEY: 'e__P8Da15NkrcnL5XBRH_g',
        EMAIL_FROM:  'info@eurovetrinaespositori.it',
        EMAIL_FROM_NAME:  'Eurovetrina',
        CLOUDINARY_CLOUD_NAME: 'plurimedia',
        CLOUDINARY_APY_KEY: '323486727446464',
        CLOUDINARY_API_SECRET: 'X7npZml3pZ2LXwqNCjXkvlt48xQ',
        PAYPAL_URL: 'https://www.paypal.com/cgi-bin/webscr',
        PAYPAL_LOGO: 'http://res.cloudinary.com/plurimedia/image/upload/v1492680385/logo-eurovetrina-blu_gfiako.png',
        PAYPAL_MAIL: 'info@eurovetrinaespositori.it',
        PAGINATE: 12,
        verbose: false
    },
    test: {
        TITLE: 'Eurovetrina (TEST)',
        URL: 'https://eurovetrina-ecommerce-test.herokuapp.com/',
        MONGO: process.env.MONGOURL, // 'mongodb://plurimedia:Canonico27!@ds145780.mlab.com:45780/heroku_dzft4j37',
        AUTH0_TOKEN: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5UZzBSalF6UlRVMk1qRTNSRE5CTWtFMU1UZEZORFk0TmtGRE1qVkVOMFExUXpReE16YzJPQSJ9.eyJpc3MiOiJodHRwczovL3BsdXJpbWVkaWEuZXUuYXV0aDAuY29tLyIsInN1YiI6IkVSWldkYzJ0UUt0MFlkVzBldWk3MXZNcUNMbElsaW5uQGNsaWVudHMiLCJhdWQiOiJodHRwczovL3BsdXJpbWVkaWEuZXUuYXV0aDAuY29tL2FwaS92Mi8iLCJpYXQiOjE2NDQ5MzM5MzIsImV4cCI6MTY1MTMzMzkzMiwiYXpwIjoiRVJaV2RjMnRRS3QwWWRXMGV1aTcxdk1xQ0xsSWxpbm4iLCJzY29wZSI6InJlYWQ6Y2xpZW50X2dyYW50cyBjcmVhdGU6Y2xpZW50X2dyYW50cyBkZWxldGU6Y2xpZW50X2dyYW50cyB1cGRhdGU6Y2xpZW50X2dyYW50cyByZWFkOnVzZXJzIHVwZGF0ZTp1c2VycyBkZWxldGU6dXNlcnMgY3JlYXRlOnVzZXJzIHJlYWQ6dXNlcnNfYXBwX21ldGFkYXRhIHVwZGF0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgZGVsZXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBjcmVhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGNyZWF0ZTp1c2VyX3RpY2tldHMgcmVhZDpjbGllbnRzIHVwZGF0ZTpjbGllbnRzIGRlbGV0ZTpjbGllbnRzIGNyZWF0ZTpjbGllbnRzIHJlYWQ6Y2xpZW50X2tleXMgdXBkYXRlOmNsaWVudF9rZXlzIGRlbGV0ZTpjbGllbnRfa2V5cyBjcmVhdGU6Y2xpZW50X2tleXMgcmVhZDpjb25uZWN0aW9ucyB1cGRhdGU6Y29ubmVjdGlvbnMgZGVsZXRlOmNvbm5lY3Rpb25zIGNyZWF0ZTpjb25uZWN0aW9ucyByZWFkOnJlc291cmNlX3NlcnZlcnMgdXBkYXRlOnJlc291cmNlX3NlcnZlcnMgZGVsZXRlOnJlc291cmNlX3NlcnZlcnMgY3JlYXRlOnJlc291cmNlX3NlcnZlcnMgcmVhZDpkZXZpY2VfY3JlZGVudGlhbHMgdXBkYXRlOmRldmljZV9jcmVkZW50aWFscyBkZWxldGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGNyZWF0ZTpkZXZpY2VfY3JlZGVudGlhbHMgcmVhZDpydWxlcyB1cGRhdGU6cnVsZXMgZGVsZXRlOnJ1bGVzIGNyZWF0ZTpydWxlcyByZWFkOmVtYWlsX3Byb3ZpZGVyIHVwZGF0ZTplbWFpbF9wcm92aWRlciBkZWxldGU6ZW1haWxfcHJvdmlkZXIgY3JlYXRlOmVtYWlsX3Byb3ZpZGVyIGJsYWNrbGlzdDp0b2tlbnMgcmVhZDpzdGF0cyByZWFkOnRlbmFudF9zZXR0aW5ncyB1cGRhdGU6dGVuYW50X3NldHRpbmdzIHJlYWQ6bG9ncyByZWFkOnNoaWVsZHMgY3JlYXRlOnNoaWVsZHMgZGVsZXRlOnNoaWVsZHMgdXBkYXRlOnRyaWdnZXJzIHJlYWQ6dHJpZ2dlcnMgcmVhZDpncmFudHMgZGVsZXRlOmdyYW50cyByZWFkOmd1YXJkaWFuX2ZhY3RvcnMgdXBkYXRlOmd1YXJkaWFuX2ZhY3RvcnMgcmVhZDpndWFyZGlhbl9lbnJvbGxtZW50cyBkZWxldGU6Z3VhcmRpYW5fZW5yb2xsbWVudHMgY3JlYXRlOmd1YXJkaWFuX2Vucm9sbG1lbnRfdGlja2V0cyByZWFkOnVzZXJfaWRwX3Rva2VucyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.DSiStmCJ89e4RQbOC5Q9MGuoUeg22Rh9KCUH7N2Fqc8g0mgynSOICIgtqLExTqqv8ZZjtdN9U83vE5NfBpr-RYzTY9rcOHtoaEq7mNgACTLf-M02pyjPGFJ3_Ut74dcWJkuE2EjgRRLncMiso8nnjfwQYqXN6TYlyrJOZiiuGqWykUc9Tes0fZOpJHOkLLOxaOlvUsQg4omuFQ7vY8P6lafYUup-gTE7dNyAIsbSIazOo5u325UKbzxD6HTNDX_nCo4J8KJAAsh5yMm6dc7DljAzDtEyR6l-LYe1cXi8u8ORjM9F8B5qnxk8HWt6YwSoDia-jfaAqgwkO-PA9YhJpg',
        AUTH0_CLIENT_SECRET: 'PJnQ9vHHe0zHsubYU1EbbOzw6NFt1ABl3yQmxbGi8JFuK6SXJ49aQP3KEOyCCdG3',
        AUTH0_CLIENT_ID: 'u04viEQz2xqLW0Q81EE4n4Jvlxf1WKoc',
        AUTH0_MM_CLIENT_SECRET: '08wu9kdgjSIXNzQCU7Jjp7BMsuIaJNvjF6SziYiF79HI3mqpM64e_XrgY2tkOYgM',
        AUTH0_MM_CLIENT_ID: 'lZoGjVY5Nuu1BM3mJSbMQI45U7fTo4VI',
        AUTH0_CONNECTION: 'eurovetrina-ecommerce-prod',
        // AUTH0_CLIENT_SECRET: '5CuL6nxIOEESm1TAiG8rgfCCIfTR2IC43_szX6waaRvfhwO-O3nh9Iq8T5BwRvFg',
        // AUTH0_CLIENT_ID: 'zr9nZoOeNf0sfmQ5LUaprfbmysEFFI1t',
        // AUTH0_CONNECTION: 'eurovetrina-ecommerce-test',
        AUTH0_CALLBACK_URL: 'https://eurovetrina-ecommerce-test.herokuapp.com/auth0callback',
        AUTH0_DOMAIN: 'plurimedia.eu.auth0.com',
        MANDRILL_API_KEY: 'e__P8Da15NkrcnL5XBRH_g',
        EMAIL_FROM:  'matteo.leoni@plurimedia.it',
        EMAIL_FROM_NAME:  'Eurovetrina (TEST)',
        CLOUDINARY_CLOUD_NAME: 'plurimedia',
        CLOUDINARY_APY_KEY: '323486727446464',
        CLOUDINARY_API_SECRET: 'X7npZml3pZ2LXwqNCjXkvlt48xQ',
        PAYPAL_URL: 'https://www.sandbox.paypal.com/cgi-bin/webscr',
        PAYPAL_LOGO: 'http://res.cloudinary.com/plurimedia/image/upload/v1492680385/logo-eurovetrina-blu_gfiako.png',
        PAYPAL_MAIL: 'plurimedia.web@plurimedia.it',
        PAGINATE: 12,
        verbose: true
    },
    dev: {
        TITLE: 'Eurovetrina (DEV)',
        URL: 'http://localhost:3000/',
        MONGO: process.env.MONGOURL, // 'mongodb://localhost/eurovetrina_ecommerce',
        AUTH0_CLIENT_SECRET: '5CuL6nxIOEESm1TAiG8rgfCCIfTR2IC43_szX6waaRvfhwO-O3nh9Iq8T5BwRvFg',
        AUTH0_CLIENT_ID: 'zr9nZoOeNf0sfmQ5LUaprfbmysEFFI1t',
        AUTH0_CONNECTION: 'eurovetrina-ecommerce-test',
        AUTH0_CALLBACK_URL: 'http://localhost:3000/auth0callback',
        AUTH0_DOMAIN: 'plurimedia.eu.auth0.com',
        AUTH0_TOKEN: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5UZzBSalF6UlRVMk1qRTNSRE5CTWtFMU1UZEZORFk0TmtGRE1qVkVOMFExUXpReE16YzJPQSJ9.eyJpc3MiOiJodHRwczovL3BsdXJpbWVkaWEuZXUuYXV0aDAuY29tLyIsInN1YiI6IkVSWldkYzJ0UUt0MFlkVzBldWk3MXZNcUNMbElsaW5uQGNsaWVudHMiLCJhdWQiOiJodHRwczovL3BsdXJpbWVkaWEuZXUuYXV0aDAuY29tL2FwaS92Mi8iLCJpYXQiOjE2NDQ5MzM5MzIsImV4cCI6MTY1MTMzMzkzMiwiYXpwIjoiRVJaV2RjMnRRS3QwWWRXMGV1aTcxdk1xQ0xsSWxpbm4iLCJzY29wZSI6InJlYWQ6Y2xpZW50X2dyYW50cyBjcmVhdGU6Y2xpZW50X2dyYW50cyBkZWxldGU6Y2xpZW50X2dyYW50cyB1cGRhdGU6Y2xpZW50X2dyYW50cyByZWFkOnVzZXJzIHVwZGF0ZTp1c2VycyBkZWxldGU6dXNlcnMgY3JlYXRlOnVzZXJzIHJlYWQ6dXNlcnNfYXBwX21ldGFkYXRhIHVwZGF0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgZGVsZXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBjcmVhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGNyZWF0ZTp1c2VyX3RpY2tldHMgcmVhZDpjbGllbnRzIHVwZGF0ZTpjbGllbnRzIGRlbGV0ZTpjbGllbnRzIGNyZWF0ZTpjbGllbnRzIHJlYWQ6Y2xpZW50X2tleXMgdXBkYXRlOmNsaWVudF9rZXlzIGRlbGV0ZTpjbGllbnRfa2V5cyBjcmVhdGU6Y2xpZW50X2tleXMgcmVhZDpjb25uZWN0aW9ucyB1cGRhdGU6Y29ubmVjdGlvbnMgZGVsZXRlOmNvbm5lY3Rpb25zIGNyZWF0ZTpjb25uZWN0aW9ucyByZWFkOnJlc291cmNlX3NlcnZlcnMgdXBkYXRlOnJlc291cmNlX3NlcnZlcnMgZGVsZXRlOnJlc291cmNlX3NlcnZlcnMgY3JlYXRlOnJlc291cmNlX3NlcnZlcnMgcmVhZDpkZXZpY2VfY3JlZGVudGlhbHMgdXBkYXRlOmRldmljZV9jcmVkZW50aWFscyBkZWxldGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGNyZWF0ZTpkZXZpY2VfY3JlZGVudGlhbHMgcmVhZDpydWxlcyB1cGRhdGU6cnVsZXMgZGVsZXRlOnJ1bGVzIGNyZWF0ZTpydWxlcyByZWFkOmVtYWlsX3Byb3ZpZGVyIHVwZGF0ZTplbWFpbF9wcm92aWRlciBkZWxldGU6ZW1haWxfcHJvdmlkZXIgY3JlYXRlOmVtYWlsX3Byb3ZpZGVyIGJsYWNrbGlzdDp0b2tlbnMgcmVhZDpzdGF0cyByZWFkOnRlbmFudF9zZXR0aW5ncyB1cGRhdGU6dGVuYW50X3NldHRpbmdzIHJlYWQ6bG9ncyByZWFkOnNoaWVsZHMgY3JlYXRlOnNoaWVsZHMgZGVsZXRlOnNoaWVsZHMgdXBkYXRlOnRyaWdnZXJzIHJlYWQ6dHJpZ2dlcnMgcmVhZDpncmFudHMgZGVsZXRlOmdyYW50cyByZWFkOmd1YXJkaWFuX2ZhY3RvcnMgdXBkYXRlOmd1YXJkaWFuX2ZhY3RvcnMgcmVhZDpndWFyZGlhbl9lbnJvbGxtZW50cyBkZWxldGU6Z3VhcmRpYW5fZW5yb2xsbWVudHMgY3JlYXRlOmd1YXJkaWFuX2Vucm9sbG1lbnRfdGlja2V0cyByZWFkOnVzZXJfaWRwX3Rva2VucyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.DSiStmCJ89e4RQbOC5Q9MGuoUeg22Rh9KCUH7N2Fqc8g0mgynSOICIgtqLExTqqv8ZZjtdN9U83vE5NfBpr-RYzTY9rcOHtoaEq7mNgACTLf-M02pyjPGFJ3_Ut74dcWJkuE2EjgRRLncMiso8nnjfwQYqXN6TYlyrJOZiiuGqWykUc9Tes0fZOpJHOkLLOxaOlvUsQg4omuFQ7vY8P6lafYUup-gTE7dNyAIsbSIazOo5u325UKbzxD6HTNDX_nCo4J8KJAAsh5yMm6dc7DljAzDtEyR6l-LYe1cXi8u8ORjM9F8B5qnxk8HWt6YwSoDia-jfaAqgwkO-PA9YhJpg',
        MANDRILL_API_KEY: 'e__P8Da15NkrcnL5XBRH_g',
        EMAIL_FROM:  'matteo.leoni@plurimedia.it',
        EMAIL_FROM_NAME:  'Eurovetrina (TEST)',
        CLOUDINARY_CLOUD_NAME: 'plurimedia',
        CLOUDINARY_APY_KEY: '323486727446464',
        CLOUDINARY_API_SECRET: 'X7npZml3pZ2LXwqNCjXkvlt48xQ',
        PAYPAL_URL: 'https://www.sandbox.paypal.com/cgi-bin/webscr',
        PAYPAL_LOGO: 'http://res.cloudinary.com/plurimedia/image/upload/v1492680385/logo-eurovetrina-blu_gfiako.png',
        PAYPAL_MAIL: 'plurimedia.web@plurimedia.it',
        PAGINATE: 12,
        verbose: true
    }
}


if (process.env.ambiente == 'prod'){
    module.exports = config.production;
}
else if (process.env.ambiente == 'test') {
    module.exports = config.test;
}
else {
    module.exports = config.dev;
}
