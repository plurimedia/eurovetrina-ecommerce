"use strict";

var CONFIG = require('./config.js'),
    UTILS = require(process.cwd() + '/server/utils'),
    _ = require('lodash'),
    express = require('express'),
    cors = require('cors'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    MongoStore = require('connect-mongodb-session')(session),
    // Auth0Strategy = require('passport-auth0'),
    // passport = require('passport'),
    helmet = require('helmet'), // security tool
    exphbs = require('express-handlebars'), // wrapper express per handelbars su express
    helpers = require('handlebars-helpers')(), // collezione di helpers per handlebars
    repeatHelper = require('handlebars-helper-repeat'),
    i18n = require('i18n'), // i18n
    mongoose = require('mongoose'), // mongoose wrapper for mongo
    bodyParser = require('body-parser'), // processa i corpi delle richieste e li mette in req.body
    mongoSanitize = require('express-mongo-sanitize'), // previene le injection
    logger = require('./server/logger'),
    AliasSchema = require( process.cwd() + '/server/models/alias'),
    MenuCategorieSchema = require( process.cwd() + '/server/models/menuCategorie'),
    ProdottoSchema = require( process.cwd() + '/server/models/prodotto'),
    CategoriaSchema = require( process.cwd() + '/server/models/categoria'),
    CategorieEvidenzaSchema = require( process.cwd() + '/server/models/categorieEvidenza'),
    OrdineSchema = require( process.cwd() + '/server/models/ordine'),
    ContattoSchema = require( process.cwd() + '/server/models/contatto'),
    auth0 = require( process.cwd() + '/server/middleware/auth0'), // chiede un token ad auth0
    dispatcher = require( process.cwd() + '/server/middleware/dispatcher'), // smista gli url
    adminApi = require('./server/api/admin'), // api admin
    backendRoutes = require('./server/routes/backend'), // routes del backend
    frontendRoutes = require('./server/routes/frontend'), // routes parte pubblica
    app = express(),
    fs = require('fs'),
    mongoUrl,   // Stringa di connessione per Mongo
    certFileDB; // Certificato SSL

require('dotenv').config()

logger.verbose('App started, configuration: %j', CONFIG);

/* @@@@@@ mongo @@@@@@ */
/* certFileDB = fs.readFileSync('./ibm_mongodb_ca.pem'); // Carica certificato SSL
console.log('certFileDB', certFileDB)
if (process.env.MONGOUSERNAME && process.env.MONGOPWD && process.env.MONGOHOST && process.env.MONGODBNAME) {
    mongoUrl = "mongodb://" + process.env.MONGOUSERNAME +
                    ":" + process.env.MONGOPWD +
                    "@" + process.env.MONGOHOST +
                    "/" + process.env.MONGODBNAME

    if (process.env.MONGOOPTIONS) {
        var mongoCliOptions = process.env.MONGOOPTIONS
        // Quest'applicazione utilizza un container Docker custom, pertanto alcuni caratteri nelle variabili globali
        // vengono trascritti nel codice unicode ed è impossibile l'utilizzo del backslash (\) come funzione
        // di escape.
        mongoCliOptions  = mongoCliOptions.replace(/\\u0026/g, "&");
        mongoUrl = mongoUrl + "?" + mongoCliOptions
    }
} else { */
    mongoUrl = process.env.MONGOURL // Variabile impostata nei config.js
//}

// mongoose.connect(mongoUrl, {useMongoClient: true}, function (err) {
console.log('mongoUrl', mongoUrl)
mongoose.connect(mongoUrl, {}, function (err) {
    if (err) throw err;

    logger.verbose('connesso a: %j', mongoUrl);
});

if (!process.env.NODE_ENV) mongoose.set('debug', true);

/* SEZIONE PASSPORT */
const { passport, authApi } = require('./server/auth/setup/setup-strategy-auth0')
const { sess } = require('./server/auth/setup/setup-session-mongodb')

app.use(session(sess))
app.use(passport.initialize())
app.use(passport.session())

app.use('/api/auth', authApi)
/* SEZIONE PASSPORT */

/*var strategy = new Auth0Strategy({
    domain:       CONFIG.AUTH0_DOMAIN,
    clientID:     CONFIG.AUTH0_CLIENT_ID,
    clientSecret: CONFIG.AUTH0_CLIENT_SECRET,
    callbackURL:  CONFIG.AUTH0_CALLBACK_URL
  }, function(accessToken, refreshToken, extraParams, profile, done) {
    return done(null, profile);
  });

passport.use(strategy);

// This can be used to keep a smaller payload
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});


app.use(passport.initialize());
app.use(passport.session());*/
app.use(cors())

i18n.configure({
    locales:['it','en'],
    directory: __dirname + '/i18n',
    cookie: 'ev_i18n',
    defaultLocale: 'it'
});

/* app.use(session({
    secret: 'sssshhhhhh',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 180 * 60 * 1000 },
    store: new MongoStore({ uri: mongoUrl, collection: 'sessions' }) // memorizza la sessione nella collection session di mongo
})); */

app.use(function (req,res,next){
  res.locals.session = req.session
  next()
})

// proteggi le rotte del backend
app.use('/admin',function (req,res,next) {
    try {
        if (req.session.passport.user.app_metadata.roles.indexOf('admin')!==-1 || req.session.passport.user.app_metadata.roles.indexOf('redattore')!==-1){
            next()
        }
    }
    catch(err) {
        logger.error('Non autorizzato ad accedere a %s', req.path)
        res.redirect('/api/auth/login')
    }
})

// proteggi gli ordini
app.use('/api/secure/ordine',function (req,res,next){
    try {
        if (req.session.passport.user.app_metadata.roles.indexOf('admin')!==-1 || req.session.passport.user.app_metadata.roles.indexOf('redattore')!==-1){
            next()
        }
    }

    catch(err) {
        logger.error('Non autorizzato ad accedere a %s', req.path)
        res.status(403).send('Non autorizzato')
    }
})

// use handebars
var hbs = exphbs.create({
    extname: '.hbs',
    defaultLayout: 'base',
    layoutsDir: './server/views/layouts',
    partialsDir: [
        './server/views/partials/'
    ],
    helpers: _.extend(helpers,{
        i18n: function() { return i18n.__.apply(this, arguments); },
        cloudinary : function (id,transformation) {
            return UTILS.cloudinaryUrl(id,transformation)
        },
        repeat: repeatHelper,
        euro: function (value) {
            return UTILS.euro(value)
        },
        json: function (value) {
            return JSON.stringify(value)
        }
    })
  });

app.engine('.hbs', hbs.engine);
app.set('views', './server/views');
app.set('view engine', '.hbs');

app.use(cookieParser());

app.use(i18n.init, function(req,res,next){
    if (!req.cookies.ev_i18n){
        i18n.setLocale('it')
        res.cookie('ev_i18n',i18n.getLocale(),{ maxAge: 900000, httpOnly: true })
    }

    next();
});

app.use(helmet({ contentSecurityPolicy: false }))
app.use(express.static('client'));
app.use(express.static('node_modules'));
app.use(bodyParser.json()) // parse data as json
app.use(mongoSanitize());

app.get('/testpaypal', function(req,res,next){
  console.log('!!')
  var request = require('request')
  request('https://tlstest.paypal.com/', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        res.send(body)
    }
   })
})

app.use('/api/user',auth0)
// smistamento url
app.use(dispatcher)
app.use('/api', adminApi);
app.use('/admin', backendRoutes);
app.use('/', frontendRoutes);

// listen on...
app.listen(process.env.PORT || 3000);
